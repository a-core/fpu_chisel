// SPDX-License-Identifier: Apache-2.0

// Initially written by: Misbah Memon (misbah.memon@aalto.fi), 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._

// Floating Point Comparator and Min/Max Module
class SPFPComp extends FPUOp {

  
  val mantA = io.in.bits.a.mantissa
  val mantB = io.in.bits.b.mantissa
  val expA  = io.in.bits.a.exponent
  val expB  = io.in.bits.b.exponent
  val signA = io.in.bits.a.sign
  val signB = io.in.bits.b.sign
  var ff    = WireDefault(VecInit(Seq.fill(8)(0.U(1.W))))
  ff := io.in.bits.fcsr 
 
 
  val isMin   = ( io.in.bits.op === FPUOpcode.FMIN_S) 
  val zeroA   = io.in.bits.fflags(6) && io.in.bits.fflags(9)
  val zeroB   = io.in.bits.fflags(7) && io.in.bits.fflags(10)
  val aNaN    = io.in.bits.fflags(3) && io.in.bits.fflags(0)
  val bNaN    = io.in.bits.fflags(4) && io.in.bits.fflags(1)
  val maxExpA = io.in.bits.fflags(3)
  val maxExpB = io.in.bits.fflags(4)

   /**
	 FMIN_S & FMAX_S 
	 if operand A is sNaN -> output B
	 if operand B is sNaN -> output A
	 if both are sNaN     -> output cNaN (ox7fc00000)
         if signA < signB => A for Max
	 if signA > signB => B for Max
	 if signA === signB && signA === 1.U (negative values) 
	 if signA === signB && exponentA < exponentB => A for MAX
	 if signA === signB && exponentA > exponentB => B for Max
	 if signA === signB && exponentA === exponentB && exponentA < exponentB => A for Max
	 if signA === signB && exponentA === exponentB && exponentA > exponentB => B for Max
	 if signA === signB && exponentA === exponentB && mantissaA === mantissaB => A for Max	
   */

  io.out.bits.result.mantissa := 0.U
  io.out.bits.result.exponent := 0.S
  io.out.bits.result.sign := 0.U
  

  when ((io.in.bits.op === FPUOpcode.FMIN_S) || (io.in.bits.op === FPUOpcode.FMAX_S)) {    
    when (aNaN || bNaN) {
      when (maxExpA && maxExpB) {
        when (~io.in.bits.a.mantissa(22) || ~io.in.bits.b.mantissa(22)) {
          ff(FaultFlag.NV.asUInt()) := 1.U  // sNaN
        } // output is cNaN
        io.out.bits.result.mantissa := 4194304.U 
        io.out.bits.result.exponent := 128.S
        io.out.bits.result.sign     := 0.U
      } .elsewhen(maxExpB) {
        when(~io.in.bits.b.mantissa(22) ) {
          ff(FaultFlag.NV.asUInt()) := 1.U // sNaN
        } // output is A
        io.out.bits.result <> io.in.bits.a
      } .elsewhen(maxExpA) {
        when(~io.in.bits.a.mantissa(22)) {
          ff(FaultFlag.NV.asUInt()) := 1.U // sNaN
        } // output is B
        io.out.bits.result <> io.in.bits.b
      } 
    } .elsewhen(signA.asBool && ~(signB.asBool)) { // A is negative, B is positive
      io.out.bits.result := Mux(isMin, io.in.bits.a, io.in.bits.b)
    } .elsewhen(signB.asBool && ~(signA.asBool)) { // B is negative, A is positive
      io.out.bits.result := Mux(isMin, io.in.bits.b, io.in.bits.a)
    } .elsewhen(signB.asBool && signA.asBool) { // Both are negative
      when(expA === expB) {
        when(mantA === mantB || mantA < mantB) {
          io.out.bits.result := Mux(isMin, io.in.bits.b, io.in.bits.a) 
        } .otherwise {
          io.out.bits.result := Mux(isMin, io.in.bits.a, io.in.bits.b)
        }
      } .elsewhen( expA > expB) {
        io.out.bits.result := Mux(isMin, io.in.bits.a, io.in.bits.b)
      } .otherwise {
        io.out.bits.result := Mux(isMin, io.in.bits.b, io.in.bits.a)
      }
    } .elsewhen(~signA.asBool && ~signB.asBool) { // Both are positive 
      when(expA === expB) {
        when(mantA === mantB || mantA > mantB) {
          io.out.bits.result := Mux(isMin, io.in.bits.b, io.in.bits.a)
        } .otherwise {
          io.out.bits.result := Mux(isMin, io.in.bits.a, io.in.bits.b)
        }
      } .elsewhen(expA > expB) {
        io.out.bits.result := Mux(isMin, io.in.bits.b, io.in.bits.a)
      } .otherwise {
        io.out.bits.result := Mux(isMin, io.in.bits.a, io.in.bits.b)
      }
    }
  } 

  when (io.in.bits.op === FPUOpcode.FCMP_FEQ_S) {
      /*
      0. check for NaNs
      1. check for zeros (+0 == -0)
      2. if signA === signB && exponentA === exponentB && mantissaA === mantissaB	=> 1 / true
	       else => 0 / false
      */

    when (aNaN && bNaN) {
      when (~io.in.bits.a.mantissa(22) || ~io.in.bits.b.mantissa(22)) {
        ff(FaultFlag.NV.asUInt()) := 1.U  // not valid
      }
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
    } .elsewhen(bNaN) {
      when(~io.in.bits.b.mantissa(22) ) {
        ff(FaultFlag.NV.asUInt()) := 1.U // sNaN
      } 
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
    } .elsewhen(aNaN) {
      when(~io.in.bits.a.mantissa(22)) {
        ff(FaultFlag.NV.asUInt()) := 1.U // sNaN
      }
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
    } .elsewhen(zeroA && zeroB && ~(signA === signB)){  // +0.0 === -0.0 -> FALSE
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
    } .elsewhen(signA === signB && expA === expB && mantA === mantB){
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 127.S
      io.out.bits.result.sign     := 0.U
    } .otherwise {
      io.out.bits.result.mantissa := 0.U 
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
    }  
  }

  when (io.in.bits.op === FPUOpcode.FCMP_FLT_S) {
      //FCMP.FLT.S    rs1 < rs2

      /*
      0. check for NaNs 
      1. if signA > signB => 1 / true
         if signA === signB && exponentA < exponentB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA < mantissaB => 1 / true
	       else => 0 / false
      */
    when(aNaN || bNaN) {
      //NaNs result in zeros and invalid flag
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
      ff(FaultFlag.NV.asUInt())   := 1.U  // NaN
    } .elsewhen(zeroA && zeroB) {
      // -0.0 < +0.0 -> True
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 127.S
      io.out.bits.result.sign     := 0.U
    } .otherwise {
      when(signA === signB) {
        when(~signA.asBool) { // both are positive
          when(expA === expB) {
            when(mantA === mantB) {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 0.S
              io.out.bits.result.sign     := 0.U
            } .elsewhen (mantA < mantB) {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 127.S
              io.out.bits.result.sign     := 0.U 
            }              
          } .elsewhen (expA < expB) {
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 127.S
            io.out.bits.result.sign     := 0.U
          } .otherwise {      
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 0.S
            io.out.bits.result.sign     := 0.U
          }
        } .elsewhen(signA.asBool) { // Both are negative
          when(expA === expB) {
            when(mantA > mantB) {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 127.S
              io.out.bits.result.sign     := 0.U
            } .otherwise {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 0.S
              io.out.bits.result.sign     := 0.U 
            }              
          } .elsewhen (expA > expB) {
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 127.S
            io.out.bits.result.sign     := 0.U
          } .otherwise {      
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 0.S
            io.out.bits.result.sign     := 0.U
          }
        }
      } .elsewhen(signA.asBool && ~signB.asBool) { //B is positive and A is negative
        io.out.bits.result.mantissa := 0.U
        io.out.bits.result.exponent := 127.S
        io.out.bits.result.sign     := 0.U
      } .otherwise {
        io.out.bits.result.mantissa := 0.U
        io.out.bits.result.exponent := 0.S
        io.out.bits.result.sign     := 0.U
      }
    }    
  }                

  when (io.in.bits.op === FPUOpcode.FCMP_FLE_S) {
 //FCMP.FLE.S    rs1 <= rs2

      /*
      0. check for NaNs 
      1. if signA > signB => 1 / true
         if signA === signB && exponentA < exponentB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA < mantissaB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA === mantissaB => 1 / true
	       else => 0 / false
      */
    when(aNaN || bNaN) {
      //NaNs result in zeros and invalid flag
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 0.S
      io.out.bits.result.sign     := 0.U
      ff(FaultFlag.NV.asUInt())   := 1.U  // NaN
    } .elsewhen(zeroA && zeroB) {
      // -0.0 <= +0.0 -> True
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 127.S
      io.out.bits.result.sign     := 0.U
    } .otherwise {
      when(signA === signB) {
        when(~signA.asBool) { // both are positive
          when(expA === expB) {
            when(mantA === mantB || mantA < mantB) {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 127.S
              io.out.bits.result.sign     := 0.U
            } .otherwise {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 0.S
              io.out.bits.result.sign     := 0.U 
            }              
          } .elsewhen (expA < expB) {
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 127.S
            io.out.bits.result.sign     := 0.U
          } .otherwise {      
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 0.S
            io.out.bits.result.sign     := 0.U
          }         
        } .elsewhen(signA.asBool) { // Both are negative
          when(expA === expB) {
            when(mantA < mantB ) {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 0.S
              io.out.bits.result.sign     := 0.U
            } .otherwise {
              io.out.bits.result.mantissa := 0.U
              io.out.bits.result.exponent := 127.S
              io.out.bits.result.sign     := 0.U 
            }              
          } .elsewhen (expA > expB) {
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 127.S
            io.out.bits.result.sign     := 0.U
          } .otherwise {      
            io.out.bits.result.mantissa := 0.U
            io.out.bits.result.exponent := 0.S
            io.out.bits.result.sign     := 0.U
          }
        }
      } .elsewhen(signA.asBool && ~signB.asBool) { //B is positive and A is negative
        io.out.bits.result.mantissa := 0.U
        io.out.bits.result.exponent := 127.S
        io.out.bits.result.sign     := 0.U
      } .otherwise {
       io.out.bits.result.mantissa := 0.U
       io.out.bits.result.exponent := 0.S
       io.out.bits.result.sign     := 0.U
      }
    }  
  }
  
  io.out.bits.fcsr := ff
  io.out.bits.op := io.in.bits.op
  io.in.ready := io.out.ready
  io.out.valid := io.in.valid
  io.out.bits.fflags := io.in.bits.fflags

  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED

}

// TEST
class SPFPCompTest extends Module {
  val comp = Module (new SPFPComp)
  val io = IO(new Bundle {
    val in  = Input(chiselTypeOf(comp.io.in))
    val out = Output(chiselTypeOf(comp.io.out))
  })
  // Input
  comp.io.in.bits  <> io.in.bits
  comp.io.in.valid := io.in.valid
  // Output
  io.out.ready      := 1.B
  comp.io.out.ready := io.out.ready
  io.out.bits       <> comp.io.out.bits
  io.out.valid      <> comp.io.out.valid

} 
