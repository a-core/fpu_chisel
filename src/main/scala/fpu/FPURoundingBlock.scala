// SPDX-License-Identifier:Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._

object RoundingModule extends ChiselEnum {
  val RNE,      // Round to Nearest, ties to even
      RTZ,      // Round towards Zero
      RDN,      // Round Down
      RUP,      // Round Up
      RMM,      // Round to Nearest, ties to Max Magntitude
      reserved_1,   // Reserved for future use
      reserved_2,   // Reserved for future use
      DYN,          // Dynamic Rounding Mode
      NULL
      = Value
} 


object RoundingType extends ChiselEnum {
  val FLOAT = Value(0.U)
  val UINT  = Value(1.U)
  val SINT  = Value(2.U)
  val SPEC  = Value(3.U)
}
