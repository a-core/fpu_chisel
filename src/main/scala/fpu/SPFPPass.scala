// SPDX-License-Identifier: Apache-2.0

// Initially written by: Nooa Jermilä (nooa.jermila@aalto.fi), 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


// PASSTHROUGH MODULE
class SPFPPass extends FPUOp {
  io.out.bits.result     <> io.in.bits.a
  io.out.bits.fcsr       := io.in.bits.fcsr
  io.out.bits.fflags     := io.in.bits.fflags
  io.out.bits.op         := io.in.bits.op
  io.in.ready            := io.out.ready
  io.out.valid           := io.in.valid
  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED
}
