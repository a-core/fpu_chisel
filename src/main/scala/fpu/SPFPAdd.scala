// SPDX-License-Identifier: Apache-2.0

// Initially written by:
// Misbah Memon (misbah.memon@aalto.fi)
// Nooa Jermilä (nooa.jermila@aalto.fi)
// 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


// SINGLE-PRECISION FLOATING-POINT ADDER
class SPFPAdd extends FPUOp {

  val mantA = io.in.bits.a.mantissa
  val mantB = io.in.bits.b.mantissa
  val expA  = io.in.bits.a.exponent
  val expB  = io.in.bits.b.exponent
  val signA = io.in.bits.a.sign
  val signB = io.in.bits.b.sign

  /**
    Notation:
     - prefix a -> correspond to the data in register rs1
         -  such as aNan, aInf or aZero 
     - prefix b -> correspond to the data in register rs2
         -  such as bNan, bInf or bZero 
     - prefix c -> correspond to the data in register rs3
         -  such as cNan, cInf or cZero 
     - prefix d -> correspond to the result of Mul in case of fused, otherwise rs1
          -  such as dNan, dInf or dZero 
     - suufix A -> corresponds to operand A (result of Mul in case of fused, otherwise rs1)
     - suffix B -> corresponds to operand B (the content of rs3 in case of fused, otherwise rs2)

  */
  val aNaN  = io.in.bits.fflags(3) && io.in.bits.fflags(0)
  val bNaN  = io.in.bits.fflags(4) && io.in.bits.fflags(1)
  val cNaN  = io.in.bits.fflags(5) && io.in.bits.fflags(2)
  val dNaN  = ((expA.asUInt === 128.U) && (mantA > 0.U))
  val aInf  = io.in.bits.fflags(3) && io.in.bits.fflags(9)
  val bInf  = io.in.bits.fflags(4) && io.in.bits.fflags(10)
  val cInf  = io.in.bits.fflags(5) && io.in.bits.fflags(11)
  val dInf  = ((expA.asUInt === 128.U) && (mantA === 0.U))
  val aZero = io.in.bits.fflags(6) && io.in.bits.fflags(9)
  val bZero = io.in.bits.fflags(7) && io.in.bits.fflags(10)
  val cZero = io.in.bits.fflags(8) && io.in.bits.fflags(11)
  val dZero = ((expA === -126.S) && (mantA === 0.U))
  
  // checks if the operation is type fused
  val isFused = (io.in.bits.op === FPUOpcode.FMADD_S) || 
                (io.in.bits.op === FPUOpcode.FMSUB_S) ||                         
                (io.in.bits.op === FPUOpcode.FNMADD_S) || 
                (io.in.bits.op === FPUOpcode.FNMSUB_S)
  
  // checks if the base operation is subtraction
  val isSub = (io.in.bits.op === FPUOpcode.FSUB_S) ||
              (io.in.bits.op === FPUOpcode.FMSUB_S) ||
              (io.in.bits.op === FPUOpcode.FNMSUB_S)
  
  // checks if the base operation is addition
  val isAdd = (io.in.bits.op === FPUOpcode.FADD_S) ||
              (io.in.bits.op === FPUOpcode.FMADD_S) ||
              (io.in.bits.op === FPUOpcode.FNMADD_S) 

  // checks if both operands are NaN
  val nanOne  = Mux(isFused, (cNaN && dNaN)  , (aNaN && bNaN))
  // checks if only operand A (result of mul in case of fused) is NaN
  val nanTwo  = Mux(isFused, (~cNaN && dNaN) , (aNaN && ~bNaN))
  // checks if only operand B ( the rs3 in case of fused) is NaN
  val nanThr  = Mux(isFused, (cNaN && ~dNaN) , (~aNaN && bNaN))
  // checks if one or both of the operands is Inf 
  val infOne  = Mux(isFused, (cInf || dInf)  , (aInf || bInf))
  // checks if operand A ( result of Mul in case of fused, otherwise rs1) is not Inf
  val infTwo  = Mux(isFused, ~dInf, ~aInf)
  // checks if operand B ( rs3 in case of fused, otherwise rs2) is not Inf
  val infThr  = Mux(isFused, ~cInf, ~bInf)
  // checks if one or both of the operands is Zero 
  val zeroOne = Mux(isFused, (cZero || dZero) , (aZero || bZero))
  // checks if operand A ( result of Mul in case of fused, otherwise rs1) is not Zero
  val zeroTwo = Mux(isFused, ~dZero, ~aZero)
  // checks if operand B ( rs3 in case of fused, otherwise rs2) is not Zero
  val zeroThr = Mux(isFused, ~cZero, ~bZero)

  var ff = WireDefault(VecInit(Seq.fill(8)(0.U(1.W))))
  ff := io.in.bits.fcsr

  val sign = WireDefault(0.B)

  when (nanOne) { // check operand A and operand B
    when(~mantA(22) || ~mantB(22)) {
      //sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // Invalid flag
    }
    io.out.bits.result.mantissa := Cat(1.U, mantA(21,0))
    io.out.bits.result.exponent := 128.S
    sign := signA 
  } .elsewhen(nanTwo) { // check operand A
    when (~mantA(22)) {
      //sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // Invalid
    }
    io.out.bits.result.mantissa := Cat(1.U, mantA(21,0))
    io.out.bits.result.exponent := 128.S
    sign := signA
  } .elsewhen(nanThr) { // check operand B
    when(~mantB(22)) {
      //sNaN 
      ff(FaultFlag.NV.asUInt()) := 1.U // invalid 
    }
    io.out.bits.result.mantissa := Cat(1.U, mantB(21,0))
    io.out.bits.result.exponent := 128.S
    sign := signB
  } .elsewhen((infOne) && isAdd) {
    // check A or B === Inf with addition
    ff(FaultFlag.NV.asUInt())   := 1.U // invalid
    when(infTwo) { // output B = Inf
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 128.S
      sign := signB
      ff(FaultFlag.NV.asUInt())   := 0.U 
    } .elsewhen(infThr) { // output A = Inf
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 128.S
      sign := signA
      ff(FaultFlag.NV.asUInt())   := 0.U     
    } .otherwise {  
      io.out.bits.result.mantissa := 4194304.U
      io.out.bits.result.exponent := 128.S
      sign := signA ^ signB
    }
  } .elsewhen(infOne && isSub) {
    // check A or B === Inf with subtraction
    ff(FaultFlag.NV.asUInt())   := 1.U // invalid
    when(infTwo) { // output B = Inf
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 128.S
      sign := signB ^ 1.U
      ff(FaultFlag.NV.asUInt())   := 0.U // invalid
    }.elsewhen(infThr) { // output A = Inf
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 128.S
      sign := signA
      ff(FaultFlag.NV.asUInt())   := 0.U // invalid
    } .otherwise {
      io.out.bits.result.mantissa := 4194304.U
      io.out.bits.result.exponent := 128.S
      sign := signA ^ signB
    }
  } .elsewhen(zeroOne && isAdd) {
    // check A or B === Zero with addition
    when(zeroTwo) { // output A 
      io.out.bits.result.mantissa := mantA
      io.out.bits.result.exponent := expA
      sign := signA
    } .elsewhen(zeroThr) { // output B
      io.out.bits.result.mantissa := mantB
      io.out.bits.result.exponent := expB
      sign := signB   
    } .otherwise {  
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := -126.S
      sign := 0.U
    }
  } .elsewhen(zeroOne && isSub) {
    // check A or B === Zero with subtraction
    when(zeroTwo) { // output A
      io.out.bits.result.mantissa := mantA
      io.out.bits.result.exponent := expA
      sign := signA 
    }.elsewhen(zeroThr) { // output B
      io.out.bits.result.mantissa := mantB
      io.out.bits.result.exponent := expB
      sign := ~signB
    } .otherwise {
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := -126.S
      sign := 0.B
    }
  } .otherwise {

    // OPERATION
    // 1. Determine the sign bit value
    // 2. Convert mantissae to two's complement if necessary
    // 2. Calculate the mantissa

    val compareCond 	= expA > expB
    val larger      	= Mux(compareCond, io.in.bits.a, io.in.bits.b)
    val smaller     	= Mux(compareCond, io.in.bits.b, io.in.bits.a)
    val shiftAmount 	= larger.exponent.asUInt - smaller.exponent.asUInt
    val mantAadj    	= Mux(compareCond, mantA, mantA >> shiftAmount)
    val mantBadj    	= Mux(compareCond, mantB >> shiftAmount, mantB)
    val mantissa    	= WireDefault(0.U(26.W))
    val operA       	= WireDefault(0.U(26.W))
    val operB       	= WireDefault(0.U(26.W))
    val retExp      	= WireDefault(0.B)
    val finalMantissa 	= WireDefault(0.U(26.W))


   // Determine the sign bit value
   when(isSub) { // subtraction
     when(compareCond) { // expA > expB
       sign := signA
     }.elsewhen((expA === expB) && (mantA === mantB)) {
       sign := signA & (~signB)
     }.elsewhen((expA === expB) && (mantA > mantB)) {
       sign := signA
     }.otherwise {
       sign := ~signB
     }
   }.otherwise { // addition
     when(compareCond) { // expA > expB
       sign := signA
     }.elsewhen((expA === expB) && (mantA === mantB)) {
       sign := signA & signB
     }.elsewhen((expA === expB) && (mantA > mantB)) {
       sign := signA
     }.otherwise {
       sign := signB
     }
   }

   // calculate the mantissa
   operA := Mux(signA.asBool(), (~mantAadj + 1.U), mantAadj)
   operB := Mux(signB.asBool(), (~mantBadj + 1.U), mantBadj)
   mantissa := Mux(isSub, operA +& (~operB + 1.U), operA +& operB)
   finalMantissa := Mux(sign === 1.U, ~mantissa + 1.U, mantissa)


   io.out.bits.result.mantissa := finalMantissa 
   io.out.bits.result.exponent := larger.exponent

  }

  val inv = (io.in.bits.op === FPUOpcode.FNMADD_S) ||
            (io.in.bits.op === FPUOpcode.FNMSUB_S)

  io.out.bits.result.sign := Mux(!inv, sign, sign ^ 1.B)

  io.out.bits.fcsr   := ff
  io.out.bits.op     := io.in.bits.op
  io.in.ready        := io.out.ready
  io.out.valid       := io.in.valid
  io.out.bits.fflags := io.in.bits.fflags
  
  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED

}

// TEST
class SPFPAddTest extends Module {
  val add = Module(new SPFPAdd)
  val io = IO(new Bundle {
    val in = Input(chiselTypeOf(add.io.in))
    val out = Output(chiselTypeOf(add.io.out))
  })
  // Input
  add.io.in.bits <> io.in.bits
  add.io.in.valid := io.in.valid
  // Output
  io.out.ready := 1.B
  add.io.out.ready := io.out.ready
  io.out.bits <> add.io.out.bits
  io.out.valid <> add.io.out.valid

}

