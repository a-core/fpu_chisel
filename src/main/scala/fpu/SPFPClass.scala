// SPDX-License-Identifier: Apache-2.0

// Initially written by: Misbah Memon (misbah.memon@aalto.fi), 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


 //FCLASS  Identify type of the FPU
class SPFPClass extends FPUOp {

  val signA = io.in.bits.a.sign
  val isPos = (signA === 0.U)


   /*  
      set bit in rd  		meaning		
      0 		= 	rs1 is -inf
      1 		= 	rs1 is a negative normal number      
      2 		= 	rs1 is a negative subnormal number
      3 		= 	rs1 is -0
      4 		= 	rs1 is +0
      5 		= 	rs1 is a positive subnormal number      
      6 		= 	rs1 is a positive normal number
      7 		= 	rs1 is +inf
      8 		= 	rs1 is a signaling NaN
      9 		= 	rs1 is a quiet NaN

      type			 pattern
      nNN 		= 	"b10000000????????????????????????"
      pNN 		= 	"b00000000????????????????????????" 
      nSN 		= 	"b100000000???????????????????????"
      pSN 		= 	"b000000000???????????????????????"
      nZero 	        = 	"b10000000000000000000000000000000" //0x80000000
      pZero 	        = 	"b00000000000000000000000000000000" //0x00000000 
      nInf 		= 	"b11111111100000000000000000000000" //0xff800000
      pInf 		= 	"b01111111100000000000000000000000" //0x7f800000
      sNaN 		= 	"b?111111110??????????????????????" //0x7fc00000
      qNaN 		= 	"b?111111111??????????????????????" //0x7fc00000
    */
  
  val zeroMant = io.in.bits.fflags(9) 
  val zeroExp  = io.in.bits.fflags(6) 
  val maxExp   = io.in.bits.fflags(3) 

   /**

   fflags(9) - checks if mantissa (22,0) of opernad A is zero 
   fflags(6) - checks if exponent of operand A is zero
   fflags(3) - checks if exponent of operand A is maximum

   */    
 

   // determine classes
  val isNZero      =  zeroMant && zeroExp  && ~isPos
  val isNNormal    =  ~maxExp  && ~io.in.bits.a.mantissa(23) && ~isPos && ~isNZero
  val isNSubnormal =  zeroExp  && io.in.bits.a.mantissa(23) && ~isPos
  val isNInf       =  maxExp   && zeroMant  && ~isPos
  val isPZero      =  zeroMant && zeroExp   && isPos
  val isPInf       =  maxExp   && zeroMant  && isPos
  val isPSubnormal =  zeroExp  && ~io.in.bits.a.mantissa(23) && isPos && ~isPZero
  val isPNormal    =  ~maxExp  && io.in.bits.a.mantissa(23)  && isPos
  val isSNaN       =  maxExp   && ~io.in.bits.a.mantissa(22) && ~(isPInf || isNInf)
  val isQNaN       =  maxExp   && io.in.bits.a.mantissa(22)  && ~(isPInf || isNInf) && ~isSNaN


  val zerobits = Fill(16, 0.U)

  io.out.bits.result.sign     := 0.U
  io.out.bits.result.exponent := 0.S
  
  io.out.bits.result.mantissa := Cat(zerobits, isQNaN, isSNaN, isPInf, isPNormal, isPSubnormal, isPZero, isNZero, isNSubnormal, isNNormal, isNInf)
  
  // FCLASS.S  does not set control and status flags
  io.out.bits.fflags := io.in.bits.fflags
  io.out.bits.fcsr := io.in.bits.fcsr
  io.out.bits.op := io.in.bits.op
  io.in.ready := io.out.ready
  io.out.valid := io.in.valid

  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED

}  

// TEST
class SPFPClassTest extends Module {
  val testclass  = Module(new SPFPClass)
  val preproc = Module(new FPUPreprocessor)
  val io = IO(new Bundle {
    val in  = Input(chiselTypeOf(preproc.io.in))
    val out = Output(chiselTypeOf(testclass.io.out))
  })
  // Input
  preproc.io.in.bits          <> io.in.bits
  preproc.io.in.valid         := io.in.valid
  testclass.io.in.bits.a      := preproc.io.out.bits.a
  testclass.io.in.bits.b      := preproc.io.out.bits.b
  testclass.io.in.bits.c      := preproc.io.out.bits.c
  testclass.io.in.bits.op     := preproc.io.out.bits.op
  testclass.io.in.bits.fcsr   := preproc.io.out.bits.fcsr
  testclass.io.in.bits.fflags := preproc.io.out.bits.fflags
  testclass.io.in.valid       := preproc.io.out.valid
  // Output
  io.out.ready           := 1.B
  testclass.io.out.ready := io.out.ready
  preproc.io.out.ready   := testclass.io.in.ready
  io.out.bits            <> testclass.io.out.bits
  io.out.valid           <> testclass.io.out.valid

}
  

