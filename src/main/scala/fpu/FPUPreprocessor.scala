// SPDX-License-Identifier: Apache-2.0

// Initially written by:
// Misbah Memon (misbah.memon@aalto.fi)
// Nooa Jermilä (nooa.jermila@aalto.fi)
// 2024-08-25

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._


class FPUPreprocessor extends Module {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Bundle {
      val rs1    = UInt(32.W)
      val rs2    = UInt(32.W)
      val rs3    = UInt(32.W)
      val op     = FPUOpcode()
      val fcsr   = Vec(8, UInt(1.W))
    }))
    val out = Decoupled(new Bundle {
      val a      = new MantExp(26, 8)
      val b      = new MantExp(26, 8)
      val c      = new MantExp(26, 8)
      val op     = FPUOpcode()
      val fcsr   = Vec(8, UInt(1.W))
      val fflags = UInt(12.W)
      val control = new ControlSignals()
      val stall   = new stallIO()
    })
  })


   def extractMantissaAndExponent(rs: UInt): MantExp = {
    val bundle = Wire(chiselTypeOf(io.out.bits.a))
    //check if the number is subnormal i.e. the exponent is 0
    val isSubnormal = (rs(30,23) === 0.U)
  
    // Add hidden bit and sign bit, and one extra bit for overflow
    // hidden bit is zero for subnormal numbers
    val hiddenBit = Mux(isSubnormal, 0.U, 1.U) 
    val mantissa = Cat(0.U, 0.U, hiddenBit, rs(22, 0))

    bundle.mantissa := mantissa
 
    val sqrt = (io.in.bits.op === FPUOpcode.FSQRT_S)

    // Remove bias (for normal numbers) or set exponent to -126 for subnormals
    when(~sqrt) {
      when(rs(30, 23) =/= 255.U) {
        bundle.exponent := Mux(isSubnormal, -126.S, rs(30, 23).asSInt - 127.S)
      } .otherwise {
        bundle.exponent := 127.S
      }
    } .otherwise {
      bundle.exponent := rs(30,23).asSInt
    }
    bundle.sign := rs(31)
    bundle
  }


  // Control signals
  val control = Wire(chiselTypeOf(io.out.bits.control))

  control.adder      := (io.in.bits.op === FPUOpcode.FADD_S) ||
                        (io.in.bits.op === FPUOpcode.FSUB_S)

  control.multiplier := (io.in.bits.op === FPUOpcode.FMUL_S)

  control.fused      := (io.in.bits.op === FPUOpcode.FMADD_S) ||
                        (io.in.bits.op === FPUOpcode.FMSUB_S) ||
                        (io.in.bits.op === FPUOpcode.FNMADD_S) ||
                        (io.in.bits.op === FPUOpcode.FNMSUB_S)

  control.divider    := (io.in.bits.op === FPUOpcode.FDIV_S)

  control.comparator := (io.in.bits.op === FPUOpcode.FCMP_FEQ_S) ||
                        (io.in.bits.op === FPUOpcode.FCMP_FLT_S) ||
                        (io.in.bits.op === FPUOpcode.FCMP_FLE_S) ||
                        (io.in.bits.op === FPUOpcode.FMIN_S) ||
                        (io.in.bits.op === FPUOpcode.FMAX_S)

  control.squareRoot := (io.in.bits.op === FPUOpcode.FSQRT_S)

  control.bitInject  := (io.in.bits.op === FPUOpcode.FSGNJ_S) ||
                        (io.in.bits.op === FPUOpcode.FSGNJN_S) ||
                        (io.in.bits.op === FPUOpcode.FSGNJX_S)
 
  control.classify   := (io.in.bits.op === FPUOpcode.FCLASS_S)

 
  val convert = (io.in.bits.op === FPUOpcode.FCVT_S_W) ||
                (io.in.bits.op === FPUOpcode.FCVT_S_WU)



  // Extract mantissa, exponent, and sign bit from the inputs
  val r1 = WireInit(0.U.asTypeOf(new MantExp(26, 8)))
  val r2 = WireInit(0.U.asTypeOf(new MantExp(26, 8)))
  val r3 = WireInit(0.U.asTypeOf(new MantExp(26, 8)))


  // (U)Int to float convertions
  when(convert) {
    val operation = io.in.bits.op
    val inputVal = io.in.bits.rs1
    val biasCount = WireInit(0.S(8.W))
    val shiftedBits = WireDefault(inputVal)
    when(inputVal === 0.U) {
      r1.mantissa := 0.U
      r1.exponent := -126.S
      r1.sign := 0.U
    } .elsewhen((operation === FPUOpcode.FCVT_S_W) &&
               (inputVal === "h80000000".U)) {
      r1.mantissa := 8388608.U
      r1.exponent := -97.S
      r1.sign := 0.U
    } .otherwise {
      val intA = WireInit(0.U(32.W))
      when(operation === FPUOpcode.FCVT_S_W) {
        intA := Mux(inputVal(31), (~inputVal(30, 0) + 1.U), inputVal(30, 0))
        biasCount := Cat(0.U(1.W), (30.U - PriorityEncoder(Reverse(intA(30, 0))))).asSInt
      } .otherwise {
        intA := inputVal
        biasCount := Cat(0.U(1.W), (31.U - PriorityEncoder(Reverse(intA)))).asSInt
      }
      when(biasCount >= 23.S) {
        shiftedBits := intA >> biasCount.asUInt - 23.U
     } .otherwise {
        shiftedBits := intA << 23.U - biasCount.asUInt
      }
      r1.mantissa := Cat(0.U(2.W), 1.U(1.W), shiftedBits(22, 0))
      r1.exponent := biasCount
      r1.sign := Mux(operation === FPUOpcode.FCVT_S_W, inputVal(31), 0.U)
    }     
  } .otherwise { // No convertions
    r1 := extractMantissaAndExponent(io.in.bits.rs1)
  }
  r2 := extractMantissaAndExponent(io.in.bits.rs2)
  r3 := extractMantissaAndExponent(io.in.bits.rs3)  
 


  /*
  Floating-point control and status register
    bit 0 - NX (Inexact)
    bit 1 - UF (Underflow)
    bit 2 - OF (Overflow)
    bit 3 - DZ (Divide by zero)
    bit 4 - NV (Invalid Operation)
    bit 5,6,7 - Rounding Mode
       000 RNE (Round to nearest, ties to even)
       001 RTZ (Round towards zero)
       010 RDN (Round down towards -Inf)
       011 RUP (Round Up towards +Inf)
       100 RMM (Round to nearest, ties to max magnitude) 
       101 - Reserved for future use
       110 - Reserved for future use
       111 DYN (In instructions's rm field, selects dynamic rounding mode; In rounding mode register, reserved)
  */

  /**
  Corresponding bit of fflags is asserted if the condition is true.
  ---- Invalid Operation Conditions

       -  bit 0  -> rs1 sNaN condition for mantissa      
       -  bit 1  -> rs2 sNaN condition for mantissa
       -  bit 2  -> rs3 sNaN condition for mantissa
       -  bit 3  -> Maximum Exponent A
       -  bit 4  -> Maximum Exponent B
       -  bit 5  -> Maximum Exponent C
       -  bit 6  -> Zero Exponent A
       -  bit 7  -> Zero Exponent B
       -  bit 8  -> Zero Exponent C
       -  bit 9  -> Zero mantissa A
       -  bit 10 -> Zero mantissa B
       -  bit 11 -> Zero mantissa C
  ---- Divide by Zero
       - no preprocessor flag required here, logic added in the division module
  ---- Overflow
      -- no preprocessor flag required here, logic added in postprocessor
  ---- Underflow
      -- no preprocessor flag required here, logic added in postprocessor 
  ---- Inexact
       -- no flag required here, logic added in postprocessor
  */ 

  /**
   -- check if rs is zero
      -- rs1 -> (bit 6) && (bit 9)
      -- rs2 -> (bit 7) && (bit 10)
      -- rs3 -> (bit 8) && (bit 11)
   -- check if rs is infinite
      -- rs1 -> (bit 3) && (bit 9)
      -- rs2 -> (bit 4) && (bit 10)
      -- rs2 -> (bit 5) && (bit 11)
  */

  // checks rs1 NaN condition for mantissa
  val aNaN   = (r1.mantissa(22,0) > 0.U)
  // checks  rs2  NaN condition for mantissa
  val bNaN   = (r2.mantissa(22,0) > 0.U)
  // checks if rs3 NaN condition for mantissa
  val cNaN   = (r3.mantissa(22,0) > 0.U)
  // maximum exponent for rs1 
  val maxExpA = (r1.exponent === 127.S)
  // maximum exponent for rs2 
  val maxExpB = (r2.exponent === 127.S)
  // maximum exponent for rs3
  val maxExpC = (r3.exponent === 127.S)
  // checks if exponent of rs1 is zero
  val zeroExpA = (r1.exponent === -126.S)
  // checks if exponent of rs2 is zero
  val zeroExpB = (r2.exponent === -126.S)
  // checks if exponent of rs3 is zero
  val zeroExpC = (r3.exponent === -126.S)
  // checks if mantissa rs1 is zero
  val zeroMantA = (r1.mantissa(22,0) === 0.U)
   // checks if mantissa rs2 is zero
  val zeroMantB = (r2.mantissa(22,0) === 0.U)
   // checks if mantissa rs3 is zero
  val zeroMantC = (r3.mantissa(22,0) === 0.U)
 
  // maximum valid input for Int 
  val maxInt = (0.B).asUInt 
  // maximum valid input for float
  val maxFloat = (0.B).asUInt 
  // minimum valid input for Int
  val miniInt = (0.B).asUInt
  // minimum valid input for float
  val miniFloat = (0.B).asUInt 

  // Detect stall. The pipeline will stall if the input operation is multiplication,
  // division, square root or fused operation
  val stall = control.multiplier.asBool() || control.fused.asBool() ||
              control.divider.asBool() || control.squareRoot.asBool()

 // Outputs
 io.out.bits.a      := r1
 io.out.bits.b      := r2
 io.out.bits.c      := r3
 io.out.bits.op     := io.in.bits.op
 io.out.bits.fcsr   := io.in.bits.fcsr
 io.in.ready        := io.out.ready
 io.out.valid       := io.in.valid
 io.out.bits.fflags := Cat(zeroMantC, zeroMantB, zeroMantA, zeroExpC, zeroExpB, zeroExpA, maxExpC, maxExpB, maxExpA, cNaN, bNaN, aNaN)
 io.out.bits.control <> control
 io.out.bits.stall.stall := stall
 io.out.bits.stall.pass := 1.B
 io.out.bits.stall.inProc := 0.B
 io.out.bits.stall.op := io.in.bits.op

}

// PREPROCESSOR TEST
class FPUPreprocessorTest extends Module {
    val preproc = Module(new FPUPreprocessor)
    val io = IO(new Bundle {
        val in = Input(chiselTypeOf(preproc.io.in))
        val out = Output(chiselTypeOf(preproc.io.out))
    })
    // Input
    //io.in.ready := preproc.io.in.ready
    preproc.io.in.bits <> io.in.bits
    preproc.io.in.valid := io.in.valid
    // Output
    io.out.ready := 1.B
    preproc.io.out.ready := io.out.ready
    io.out.bits <>  preproc.io.out.bits
    io.out.valid := preproc.io.out.valid

}
