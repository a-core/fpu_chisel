// SPDX-Liscence-Identifier: Apache-2.0

// chisel top module fpu
// Initially written by: Misbah Memon (misbah.memon@aalto.fi) , 2024-07-09
// Modified by: Nooa Jermilä (nooa.jermila@aalto.fi), 2024-08-25


package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

// define a bundle for pipeline
class stallIO extends Bundle {
  val stall	= Bool()
  val op	= FPUOpcode()
  val pass    = Bool()
  val inProc  = Bool()
}

// Define a control signals bundle
class ControlSignals extends Bundle {
  val adder      = Bool()
  val multiplier = Bool()
  val fused      = Bool()
  val divider    = Bool()
  val comparator = Bool()
  val squareRoot = Bool()
  val bitInject  = Bool()
  val classify   = Bool()
}

// FPU TOP-LEVEL MODULE
class FPU extends Module {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Bundle {
      val rs1  = UInt(32.W)  // source register 1
      val rs2  = UInt(32.W)  // source register 2
      val rs3  = UInt(32.W)  // source register 3
      val op   = FPUOpcode() // opcode
      val fcsr = UInt(8.W)   // status and control flag
    }))
    val out = Decoupled(new Bundle {
      val rd   = UInt(32.W)      // destination register
      val fcsr = UInt(8.W)       // status and control register
     })
    })

  // Use lowercase in the Verilog file for 
  // easier integration with TheSydeKick
  override def desiredName = "fpu"
 
  
  // Create and initialize the preprocessor, the execution unit, and the postprocessor
  val preprocessor  = Module (new FPUPreprocessor)
  val executionUnit = Module (new FPUExecutionUnit)
  val postprocessor = Module (new FPUPostprocessor(26,8))

  // Preprocessor
  preprocessor.io.in.bits.rs1  := 0.U
  preprocessor.io.in.bits.rs2  := 0.U
  preprocessor.io.in.bits.rs3  := 0.U
  preprocessor.io.in.bits.fcsr := Seq.fill(8)(0.U(1.W))
  preprocessor.io.in.bits.op   := FPUOpcode.NULL
  preprocessor.io.out.ready    := 0.B

  // Execution unit
  executionUnit.io.in.bits.a.mantissa 	      := 0.U
  executionUnit.io.in.bits.a.exponent 	      := 0.S
  executionUnit.io.in.bits.a.sign     	      := 0.U
  executionUnit.io.in.bits.b.mantissa 	      := 0.U
  executionUnit.io.in.bits.b.exponent 	      := 0.S
  executionUnit.io.in.bits.b.sign     	      := 0.U
  executionUnit.io.in.bits.c.mantissa 	      := 0.U
  executionUnit.io.in.bits.c.exponent 	      := 0.S
  executionUnit.io.in.bits.c.sign     	      := 0.U
  executionUnit.io.in.bits.fcsr       	      := Seq.fill(8)(0.U(1.W))
  executionUnit.io.in.bits.op         	      := FPUOpcode.NULL
  executionUnit.io.in.valid           	      := 0.B
  executionUnit.io.out.ready          	      := 0.B
  executionUnit.io.in.bits.fflags     	      := 0.U
  executionUnit.io.in.bits.control.adder      := 0.B
  executionUnit.io.in.bits.control.multiplier := 0.B
  executionUnit.io.in.bits.control.fused      := 0.B
  executionUnit.io.in.bits.control.divider    := 0.B
  executionUnit.io.in.bits.control.comparator := 0.B
  executionUnit.io.in.bits.control.squareRoot := 0.B
  executionUnit.io.in.bits.control.bitInject  := 0.B
  executionUnit.io.in.bits.control.classify   := 0.B
  executionUnit.io.in.bits.stall.stall        := 0.B
  executionUnit.io.in.bits.stall.pass         := 0.B
  executionUnit.io.in.bits.stall.inProc       := 0.B
  executionUnit.io.in.bits.stall.op           := FPUOpcode.NULL

  // Postprocessor
  postprocessor.io.in.bits.result.mantissa := 0.U
  postprocessor.io.in.bits.result.exponent := 0.S
  postprocessor.io.in.bits.result.sign     := 0.U
  postprocessor.io.in.bits.fcsr            := Seq.fill(8)(0.U(1.W))
  postprocessor.io.in.bits.op              := FPUOpcode.NULL 
  postprocessor.io.in.valid                := 0.B
  postprocessor.io.in.bits.fflags          := 0.U
  postprocessor.io.in.bits.stall.stall     := 0.B
  postprocessor.io.in.bits.stall.pass      := 0.B
  postprocessor.io.in.bits.stall.inProc    := 0.B
  postprocessor.io.in.bits.stall.op        := FPUOpcode.NULL
 

  // connect the input ready-valid signal to preprocessor-in
  // preprocessor receives valid signal(valid) and valid data(bits)
  preprocessor.io.in.valid    := io.in.valid 
  preprocessor.io.in.bits.op  := io.in.bits.op
  preprocessor.io.in.bits.rs1 := io.in.bits.rs1
  preprocessor.io.in.bits.rs2 := io.in.bits.rs2
  preprocessor.io.in.bits.rs3 := io.in.bits.rs3
  for(i <- 0 until 8) {
    preprocessor.io.in.bits.fcsr(i) := io.in.bits.fcsr(i)
  }
  preprocessor.io.out.ready := executionUnit.io.in.ready


  // preprocessorReg between the preprocessor and the execution unit
  val preprocessorReg          = RegEnable(preprocessor.io.out.bits, preprocessor.io.out.valid)
  val preprocessorValidReg     = RegInit(false.B)
  preprocessorValidReg         := preprocessor.io.out.valid
  preprocessorReg.stall.pass   := executionUnit.io.out.bits.stall.pass
  preprocessorReg.stall.op     := executionUnit.io.out.bits.stall.op
  preprocessorReg.stall.inProc := executionUnit.io.out.bits.stall.inProc

  // Update stall bits
  when(preprocessor.io.out.bits.stall.stall && ~preprocessorReg.stall.stall) {
    preprocessorReg.stall.stall  := 1.B
    preprocessorReg.stall.pass   := 1.B
    preprocessorReg.stall.op	 := preprocessor.io.out.bits.op
  }. elsewhen(~preprocessor.io.out.bits.stall.stall && preprocessorReg.stall.stall) {
    preprocessorReg.stall.stall  := 1.B
    preprocessorReg.stall.pass   := 0.B
  }

 // Initial values for the stall bits
  when(~io.in.valid) {
    preprocessorReg.stall.stall  := 0.B
    preprocessorReg.stall.pass   := 1.B
    preprocessorReg.stall.inProc := 0.B
    preprocessorReg.stall.op     := FPUOpcode.NULL
    preprocessorReg.stall.inProc := 0.B
  }

  // Execution unit inputs
  executionUnit.io.in.bits   <> preprocessorReg
  executionUnit.io.in.valid  := preprocessorValidReg
  executionUnit.io.out.ready := postprocessor.io.in.ready

  // executorReg between the execution unit and the postprocessor
  val executorReg       = RegEnable(executionUnit.io.out.bits, executionUnit.io.out.valid && postprocessor.io.in.ready)
  val executorValidReg  = RegInit(false.B)
  executorValidReg     := executionUnit.io.out.valid

  // Determine whether end the pipeline stall
  val endStall = executorValidReg && ~preprocessorReg.stall.pass && preprocessorReg.stall.stall

  io.in.ready := preprocessor.io.in.ready

  when(endStall && ~preprocessor.io.out.bits.stall.stall) {
    preprocessorReg.stall.stall  := 0.B
    preprocessorReg.stall.pass   := 0.B
  }. elsewhen(endStall && ~executionUnit.io.out.bits.stall.inProc) {
    preprocessorReg.stall.stall  := 1.B
    preprocessorReg.stall.pass   := 1.B
    preprocessorReg.stall.op     := preprocessorReg.op
    preprocessor.io.out.ready    := 1.B
  }

  val fused = (preprocessorReg.stall.op === FPUOpcode.FMADD_S) ||
              (preprocessorReg.stall.op === FPUOpcode.FNMADD_S) ||
              (preprocessorReg.stall.op === FPUOpcode.FMSUB_S) ||
              (preprocessorReg.stall.op === FPUOpcode.FNMSUB_S)

  // Update the pass bit
  when(~preprocessorReg.stall.stall) {
    preprocessorReg.stall.pass := 1.B
  } .elsewhen(((preprocessorReg.stall.op === preprocessorReg.op) ||
              (fused && preprocessorReg.control.fused))) {
    executionUnit.io.in.bits.stall.pass := 1.B // Pipelined operations
    when((preprocessor.io.out.bits.op === preprocessorReg.op) ||
         (preprocessor.io.out.bits.control.fused && preprocessorReg.control.fused)) {
      preprocessor.io.out.ready := 1.B
    }
  }

  // Update ready signal
  when(preprocessorReg.stall.stall && ~executionUnit.io.out.bits.stall.inProc &&
       (preprocessor.io.out.bits.op === preprocessorReg.stall.op)) {
    preprocessor.io.out.ready := ~executionUnit.io.out.bits.stall.inProc
  }

  // Postprocessor inputs
  postprocessor.io.in.bits   <> executorReg
  postprocessor.io.in.valid  := executorValidReg
  postprocessor.io.out.ready := io.out.ready

  // FPU outputs
  // postprocessing unit output connected to the output of the FPU unit
  // postprocessing unit  provides valid data(bits) and asserts valid signal(valid)
  io.out.bits.rd   := postprocessor.io.out.bits.rd
  io.out.bits.fcsr := postprocessor.io.out.bits.fcsr.asUInt()
  io.out.valid     := postprocessor.io.out.valid
 
  /** 
   The fpu is ready to receive new data when the preprocessor has processed the preceding data
   controlled by the readiness of the execution unit, if it is still processing, the input ready
   should remain deserted 
  */


}

object FPU extends App { 
 (new ChiselStage).execute(
    {args.toArray},
    Seq(
      ChiselGeneratorAnnotation(() => {
        new FPU()
      }),
    )
  )
}
