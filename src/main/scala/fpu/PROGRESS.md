      
      Operation  File                	(Preprocessing, Module, Postprocessing, Test)
		
      FADD_S      SPFPAdd.scala       	 (Yes, Yes, Yes, Pass)
      FSUB_S      SPFPAdd.scala       	 (Yes, Yes, Yes, Pass)
      FMUL_S      SPFPMul.scala       	 (Yes, Yes, Yes, Pass)
      FDIV_S      SPFPDiv.scala       	 (Yes, Yes, Yes, Pass)
      FSQRT_S     SPFPSQRT.scala      	 (Yes, Yes, Yes,  -  )
      FMIN_S      SPFPComp.scala      	 (Yes, Yes, Yes, Pass)
      FMAX_S      SPFPComp.scala      	 (Yes, Yes, Yes, Pass)
      FMADD_S     SPFP(Add/Mul).scala	 (Yes, Yes, Yes,  -  )
      FMSUB_S     SPFP(Add/Mul).scala 	 (Yes, Yes, Yes,  -  )
      FNMADD_S    SPFP(Add/Mul).scala 	 (Yes, Yes, Yes,  -  )
      FNMSUB_S    SPFP(Add/Mul).scala 	 (Yes, Yes, Yes,  -  )
      FCMP_FEQ_S  SPFPComp.scala      	 (Yes, Yes, Yes, Pass)
      FCMP_FLT_S  SPFPComp.scala      	 (Yes, Yes, Yes, Pass) 
      FCMP_FLE_S  SPFPComp.scala      	 (Yes, Yes, Yes, Pass)
      FCVT_W_S    FPUPostprocessor.scala   (Yes, Yes, Yes, Pass  )
      FCVT_S_W    FPUPreprocessor.scala    (Yes, Yes, Yes, Pass  )
      FCVT_WU_S   FPUPostprocessor.scala   (Yes, Yes, Yes, Pass  )
      FCVT_S_WU   FPUPreprocessor.scala    (Yes, Yes, Yes, Pass  )
      FSGNJ_S     SPFPSGNJ.scala      	 (Yes, Yes, Yes, Pass)
      FSGNJN_S    SPFPSGNJ.scala      	 (Yes, Yes, Yes, Pass)
      FSGNJX_S    SPFPSGNJ.scala      	 (Yes, Yes, Yes, Pass)
      FCLASS_S    SPFPClass.scala     	 (Yes, Yes, Yes, Pass)

----------------------------------------------------------------------------------    

Operation Object:	      FPUOp.scala,			Works  
Preprocessing:		FPUPreprocessing.scala,		Works  
Postprocessing:		FPUPostprocessor.scala,		Works  

----------------------------------------------------------------------------------

SQRT : The output sign bit to be fixed to 0 in the sqrt module

Assumption for the conversion operations:
the msb is the sign bit, rest of the (n-1)  bits are for the significant/ integer itself. For other operations, we have the following structure, sign bit is (n-1), exponent bits are from (n-2) to (n-10) and the rest of the bits for mantissa.

The output from the postprocessor is normalised for all operations.


