// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._

class MantExp(mantWidth: Int, expWidth: Int) extends Bundle {
  /** Signed fixed-point in 2's complement format */
  val mantissa = UInt(mantWidth.W)
  /** Signed 8-bit exponent without bias */
  val exponent = SInt(expWidth.W)
  /** Sign bit */
  val sign = UInt(1.W) // 1 bit wide sign bit
}

class FPUOpIO extends Bundle {
  val in = Flipped(Decoupled(new Bundle {
    /** A operand. */
    val a = new MantExp(26, 8)
    /** B operand. Not needed for all operations, e.g. sqrt */
    val b = new MantExp(26, 8)
    /** C operand - only needed for fused multiply-add */
    val c = new MantExp(26, 8)
    /** FPU operation */
    val op = FPUOpcode()
    /** Floating-Point Control and Status Register */
    val fcsr = Vec(8, UInt(1.W))
    /** Additional Register for fcsr conditions  */
    val fflags = UInt(12.W)
  }))
  // Mantissa contains 3 extra bits for rounding
  val out = Decoupled(new Bundle {
    val result     = new MantExp(26, 8)
    val fcsr       = Vec(8, UInt(1.W))
    val fflags     = UInt(12.W) // Propogated only for Fused
    val op         = FPUOpcode()
    val mulInProc  = Bool()
    val divInProc  = Bool()
    val sqrtInProc = Bool()
  })
}

/** Enumeration of floating point unit operations */
object FPUOpcode extends ChiselEnum {
  val FADD_S,      // Single precision Floating-point addition	    ( rs1 + rs2 )
      FSUB_S,      // SPFP subtraction				    ( rs1 - rs2 )
      FMUL_S,      // SPFP multiplication			    ( rs1 * rs2 )
      FDIV_S,      // SPFP division				    ( rs1 / rs2 )
      FSQRT_S,     // SPFP square root				    ( SQRT(rs1) )
      FMIN_S,      // SPFP minimum				    MIN(rs1, rs2)
      FMAX_S,      // SPFP maximum				    MAX(rs1, rs2)
      FMADD_S,     // SPFP fused multiply addition		    ( rs1 * rs2 ) + rs3
      FMSUB_S,     // SPFP fused multiply subtraction		    ( rs1 * rs2 ) - rs3
      FNMADD_S,    // SPFP fused negative multiply addition	    -( rs1 * rs2 + rs3)
      FNMSUB_S,    // SPFP fused negative multiply subtraction	    -( rs1 * rs2 - rs3)
      FCMP_FEQ_S,  // SPFP equals 			            ( rs1 == rs2 )
      FCMP_FLT_S,  // SPFP less than 			            ( rs1 < rs2 )
      FCMP_FLE_S,  // SPFP less than OR equal 		            ( rs1 <= rs2 )
      FCVT_W_S,    // SPFP      ->   32b int
      FCVT_S_W,    // 32b int   ->   SPFP
      FCVT_WU_S,   // SPFP      ->   32b Uint
      FCVT_S_WU,   // 32b Uint  ->   SPFP
      FSGNJ_S,     // sign bit is rs2's sign bit
      FSGNJN_S,    // sign bit is opposite of r2's sign bit
      FSGNJX_S,    // sign bit is XOR of rs1 & rs2      
      FCLASS_S,    // Identify type of the SPFP
      NULL
      = Value 
}

/** Enumeration of floating point fault flag */
object FaultFlag extends ChiselEnum {
  val NX = Value(0.U)  // Inexact operation flag
  val UF = Value(1.U)  // Underflow operation flag
  val OF = Value(2.U)  // Overflow operation flag
  val DZ = Value(3.U)  // Division by zero operation flag
  val NV = Value(4.U)  // Invalid operation flag
}
/**
  * Abstract class for FPU operations.
  * Defines the IO.
  * Modules should be combinatorial - we let synthesis tools
  * do register retiming for us.
  */
abstract class FPUOp extends Module {
  val io = IO(new FPUOpIO)
}

