// SPDX-License-Identifier: Apache-2.0

// Initially written by: Misbah Memon (misbah.memon@aalto.fi), 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


// SINGLE-PRECISION FLOATING-POINT SIGN INJECTION OPERATION
class SPFPSGNJ extends FPUOp {

  /**
   FSGNJ.S     rs1 with rs2's sign bit
   FSGNJN.S    rs1 with complement of rs2's sign bit
   FSGNJX.S    rs1 with XOR of the sign bit of rs1 &  rs2
  */
  io.out.bits.result.mantissa := io.in.bits.a.mantissa 
  io.out.bits.result.exponent := io.in.bits.a.exponent

 // Computing the sign bits for different operations
  val signFSGNJ = io.in.bits.b.sign
  val signFSGNJN = ~io.in.bits.b.sign
  val signFSGNJX = io.in.bits.a.sign ^ io.in.bits.b.sign
  
  when(io.in.bits.op === FPUOpcode.FSGNJ_S) {
  io.out.bits.result.sign := signFSGNJ
  }.elsewhen (io.in.bits.op === FPUOpcode.FSGNJN_S) {
  io.out.bits.result.sign := signFSGNJN
  }.otherwise {
  io.out.bits.result.sign := signFSGNJX
  }

 // Sign Injection operations do not set any flags
  io.out.bits.fflags := io.in.bits.fflags
  io.out.bits.fcsr := io.in.bits.fcsr
  io.out.bits.op := io.in.bits.op
  io.in.ready := io.out.ready
  io.out.valid := io.in.valid

  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED

}


// TEST
class SPFPSGNJTest extends Module {
  val sgnj  = Module(new SPFPSGNJ)
  val io = IO(new Bundle {
    val in  = Input(chiselTypeOf(sgnj.io.in))
    val out = Output(chiselTypeOf(sgnj.io.out))
  })
  // Input
  sgnj.io.in.bits  <> io.in.bits
  sgnj.io.in.valid := io.in.valid
  // Output
  io.out.ready      := 1.B
  sgnj.io.out.ready := io.out.ready
  io.out.bits       <> sgnj.io.out.bits
  io.out.valid      <> sgnj.io.out.valid

}
  

