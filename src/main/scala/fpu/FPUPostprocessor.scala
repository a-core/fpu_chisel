// SPDX-License-Identifier: Apache-2.0

// Initially written by:
// Misbah Memon (misbah.memon@aalto.fi)
// Nooa Jermilä (nooa.jermila@aalto.fi)
// 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._

class FPUPostprocessor(mantWidth: Int, expWidth: Int) extends Module {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Bundle {
      val result      = new MantExp(26,8)
      val op          = FPUOpcode()
      val fcsr        = Vec(8, UInt(1.W))
      val fflags      = UInt(11.W) // Not to be propogated further
      val stall       = new stallIO()
      }))
    val out = Decoupled(new Bundle {
      val rd        = UInt(32.W)
      val fcsr      = Vec(8, UInt(1.W))
      })   
  })
  
  val ff = WireInit(io.in.bits.fcsr)

  val maxExponent = 127.S
  val minExponent = -126.S

  val finalMantissa = Wire(UInt(23.W))
  val finalExponent = Wire(SInt(expWidth.W))
  val finalSign     = Wire(UInt(1.W))
  val result        = WireDefault(0.U(32.W))
 
  val normalizedMantissa = Wire(UInt(26.W))
  val adjExponent        = Wire(SInt(8.W))
  val bitLocation        = WireInit(0.U(8.W))
  val shiftAmount        = WireInit(0.U(8.W))

  normalizedMantissa := io.in.bits.result.mantissa
  adjExponent        := io.in.bits.result.exponent

  finalMantissa := normalizedMantissa(22, 0)
  finalExponent := adjExponent + 127.S
  finalSign     := io.in.bits.result.sign
    
  val maxExp = (io.in.bits.result.exponent === maxExponent)
  val rNaN   = (io.in.bits.result.mantissa > 0.U)
  val minExp = (io.in.bits.result.exponent === minExponent)
  val isPos  = (io.in.bits.result.sign     === 0.U)

  val isSub  = (io.in.bits.op === FPUOpcode.FSUB_S ||
		io.in.bits.op === FPUOpcode.FMSUB_S ||
		io.in.bits.op === FPUOpcode.FNMSUB_S
	       )

  val maxmin = (io.in.bits.op === FPUOpcode.FMAX_S ||
                io.in.bits.op === FPUOpcode.FMIN_S)

  // Type 1: normalised and also check for overflow & underflow
  val type1 = (io.in.bits.op === FPUOpcode.FADD_S   || 
               io.in.bits.op === FPUOpcode.FSUB_S   || 
               io.in.bits.op === FPUOpcode.FMUL_S   || 
               io.in.bits.op === FPUOpcode.FDIV_S   || 
               io.in.bits.op === FPUOpcode.FMADD_S  || 
               io.in.bits.op === FPUOpcode.FMSUB_S  || 
               io.in.bits.op === FPUOpcode.FNMADD_S || 
               io.in.bits.op === FPUOpcode.FNMSUB_S 
              ) 
 
  // Type 2: normalised only
  val type2 = (io.in.bits.op === FPUOpcode.FMIN_S     || 
               io.in.bits.op === FPUOpcode.FMAX_S     || 
               io.in.bits.op === FPUOpcode.FSGNJ_S    || 
               io.in.bits.op === FPUOpcode.FSGNJN_S   ||  
               io.in.bits.op === FPUOpcode.FSGNJX_S   || 
               io.in.bits.op === FPUOpcode.FCVT_S_W   || 
               io.in.bits.op === FPUOpcode.FCVT_S_WU
              )

  // Type 3: no need for normalization
  val type3 = (io.in.bits.op === FPUOpcode.FCMP_FLT_S || 
               io.in.bits.op === FPUOpcode.FCMP_FEQ_S || 
               io.in.bits.op === FPUOpcode.FCMP_FLE_S || 
               io.in.bits.op === FPUOpcode.FCLASS_S   ||
               io.in.bits.op === FPUOpcode.FSQRT_S )

  val sqrt = (io.in.bits.op === FPUOpcode.FSQRT_S)

  // Convertion functions, no need for normalization
  val type4 = (io.in.bits.op === FPUOpcode.FCVT_W_S ||
               io.in.bits.op === FPUOpcode.FCVT_WU_S
              )
    

  // Shift the mantissa
  when(rNaN && ~type3 && ~(io.in.bits.fcsr.map(_.asBool()).reduce(_||_))) {
    bitLocation := PriorityEncoder(Reverse(io.in.bits.result.mantissa(24, 0)))
    when(bitLocation < 1.U) {
      shiftAmount := 1.U - bitLocation
      normalizedMantissa := io.in.bits.result.mantissa >> shiftAmount
      adjExponent := io.in.bits.result.exponent + Cat(0.U(1.W), shiftAmount).asSInt
    }.elsewhen(bitLocation > 1.U) {
      shiftAmount := bitLocation - 1.U
      normalizedMantissa := io.in.bits.result.mantissa << shiftAmount
      adjExponent := io.in.bits.result.exponent - Cat(0.U(1.W), shiftAmount).asSInt
    }
  }


  when(type3) {
      finalExponent := io.in.bits.result.exponent
      finalMantissa := io.in.bits.result.mantissa
      finalSign     := io.in.bits.result.sign
  } 

  when(type4) {
    val sign = WireDefault(0.U(1.W))
    sign := io.in.bits.result.sign
    val exp = io.in.bits.result.exponent
    val mant = io.in.bits.result.mantissa
    val expBias = WireDefault(0.U(10.W))
    val shiftedBits = WireDefault(0.U(32.W))

    // TODO: Implement Rounding Module to add inexact flag

    when((io.in.bits.op === FPUOpcode.FCVT_WU_S) && ~isPos && ~(maxExp && rNaN)) {
      //All are negative values except for NaN 
      ff(FaultFlag.NV.asUInt()) := 1.U // Invalid
      result := Fill(32, 0.U)
    } .elsewhen((io.in.bits.op === FPUOpcode.FCVT_WU_S) && (exp > 31.S)) {
      // Float is larger than the maximum unsigned integer
      // -> Clip to largest possible value and set invalid op flag
      // THis handdles also +Inf and NaN cases
      ff(FaultFlag.NV.asUInt()) := 1.U   // Invalid
      result := Fill(32, 1.U)
    } .elsewhen((io.in.bits.op === FPUOpcode.FCVT_W_S) && (exp > 30.S)) { // float to Int
      ff(FaultFlag.NV.asUInt()) := 1.U // Invalid
      result := Mux(sign === 0.U || (sign === 1.U && exp === 127.S && mant(22, 0) =/= 0.U), "h7fffffff".U, "h80000000".U)
    } .otherwise {
      val integer = WireDefault(0.U(32.W))
      when(exp >= 23.S) {
        integer := mant << exp.asUInt - 23.U
      } .otherwise {
        integer := mant >> 23.U - exp.asUInt
        val outbits = WireDefault(0.U(32.W))
        when(exp < 0.S) {
          outbits := mant(22, 0)
        } .otherwise {
          outbits := mant(22, 0) << exp.asUInt
        }
        when(outbits =/= 0.U) {
          ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
        }
      }
      when(io.in.bits.op === FPUOpcode.FCVT_W_S) {
        result := Mux(sign === 0.U, integer, Cat(sign, ~integer(30, 0)) + 1.U) // Float to Int
      }. otherwise {
        result := integer
      }
    }
  }

  when(type2) {
    finalMantissa := normalizedMantissa
    finalExponent := adjExponent + 127.S
    finalSign     := io.in.bits.result.sign
    when(io.in.bits.op === FPUOpcode.FMAX_S || io.in.bits.op === FPUOpcode.FMIN_S) {
      when(ff(4) === 1.U) {
        finalExponent := io.in.bits.result.exponent
        finalMantissa := io.in.bits.result.mantissa
        finalSign     := io.in.bits.result.sign
      }
    } 
  }
 
  // TODO: Add logic for sticky bit and outBit to implement IN exact flag 
  when(type1) {
    when(io.in.bits.fcsr(4) === 1.U) {
      finalMantissa := io.in.bits.result.mantissa
      finalExponent := io.in.bits.result.exponent
    }.elsewhen(io.in.bits.fcsr(3) === 1.U) {
      finalMantissa := io.in.bits.result.mantissa(22, 0)
      finalExponent := 255.S
    }.elsewhen((io.in.bits.result.mantissa === 0.U) &&
               (io.in.bits.result.exponent =/= maxExponent)) {
      finalExponent := 0.S
    } .otherwise {
      when(adjExponent > maxExponent) {
        finalMantissa := 0.U
        finalExponent := maxExponent + 127.S
        ff(FaultFlag.OF.asUInt()) := 1.U      // overflow flag
        ff(FaultFlag.NX.asUInt()) := 1.U      // inexact flag
      } .elsewhen(adjExponent < minExponent) {
        finalMantissa := 0.U
        finalExponent := minExponent + 127.S
        ff(FaultFlag.UF.asUInt())  := 1.U    // undeflow flag
        ff(FaultFlag.NX.asUInt()) := 1.U    // inexact flag
     } .otherwise {
        finalExponent := adjExponent + 127.S
        finalMantissa := normalizedMantissa  
      }
    }
    finalSign := io.in.bits.result.sign
  }

  // Test if the number is zero, inf or NaN
  when((io.in.bits.result.mantissa(23) === 0.U) &&
      (io.in.bits.result.exponent === -126.S) &&
      (type1 || type2)) {
      finalExponent := 0.S
      finalMantissa := io.in.bits.result.mantissa(22, 0)
      finalSign     := io.in.bits.result.sign
  } .elsewhen((io.in.bits.result.mantissa(23) === 0.U) &&
             (io.in.bits.result.exponent.asUInt === 128.U) &&
             (type1 || type2)) {
      finalExponent := 255.S
      finalMantissa := io.in.bits.result.mantissa(22, 0)
      finalSign     := io.in.bits.result.sign
  } .elsewhen((io.in.bits.result.mantissa(22) === 1.U) &&
	     (io.in.bits.result.exponent.asUInt === 128.U) &&
             (type1 || type2)) {
      finalExponent := 255.S
      finalMantissa := io.in.bits.result.mantissa(22, 0)
      finalSign     := io.in.bits.result.sign
  }


  // Outputs
  when(type4) {
    io.out.bits.rd := result
  }. elsewhen(io.in.bits.op === FPUOpcode.NULL) {
    io.out.bits.rd := 0.U
  } .otherwise {
    io.out.bits.rd := Cat(finalSign, finalExponent.asUInt, finalMantissa)
 }
 
  io.out.bits.fcsr := ff
  io.in.ready := io.out.ready
  io.out.valid := io.in.valid 
} 


// POSTPROCESSOR TEST
class FPUPostprocessorTest extends Module {
    val postproc = Module(new FPUPostprocessor(26,8))
    val io = IO(new Bundle {
        val in  = Input(chiselTypeOf(postproc.io.in))
        val out = Output(chiselTypeOf(postproc.io.out))
    })
   // Input
   postproc.io.in.bits   <> io.in.bits
   postproc.io.in.valid  := io.in.valid
   // Output
   io.out.ready          := 1.B
   postproc.io.out.ready := io.out.ready
   io.out.bits           <> postproc.io.out.bits
   io.out.valid          := postproc.io.out.valid
} 
