// SPDX-License-Identifier: Apache-2.0

// chisel fpu execution unit
// Initially written by: Nooa Jermilä (nooa.jermila@aalto.fi) , 2024-08-25

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._

class FPUExecutionUnit extends Module {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Bundle {
      val a       = new MantExp(26, 8)
      val b       = new MantExp(26, 8)
      val c       = new MantExp(26, 8)
      val op      = FPUOpcode()
      val fcsr    = Vec(8, UInt(1.W))
      val fflags  = UInt(12.W)
      val control = new ControlSignals()
      val stall   = new stallIO()
    }))
    val out = Decoupled(new Bundle {
      val result     = new MantExp(26, 8)
      val fcsr       = Vec(8, UInt(1.W))
      val fflags     = UInt(12.W)
      val op         = FPUOpcode()
      val stall	     = new stallIO()
    })
  })


  // Create and initialize the executor units
  val executorAdd   = Module (new SPFPAdd)	
  val executorMul   = Module (new SPFPMul)
  val executorDiv   = Module (new SPFPDiv)	
  val executorSqrt  = Module (new SPFPSQRT)
  val executorComp  = Module (new SPFPComp)	
  val executorSgn   = Module (new SPFPSGNJ)	
  val executorClass = Module (new SPFPClass)
  val passthrough   = Module (new SPFPPass)	

  val submodules = VecInit(Seq(executorAdd.io,
			       executorMul.io,
    			       executorDiv.io,
			       executorSqrt.io,
			       executorComp.io,
			       executorSgn.io,
			       executorClass.io,
			       passthrough.io
   			      ))

  for ( i <- 0 to 7){
    submodules(i).in.bits.a.mantissa := 0.U
    submodules(i).in.bits.a.exponent := 0.S
    submodules(i).in.bits.a.sign     := 0.U
    submodules(i).in.bits.b.mantissa := 0.U
    submodules(i).in.bits.b.exponent := 0.S
    submodules(i).in.bits.b.sign     := 0.U
    submodules(i).in.bits.c.mantissa := 0.U
    submodules(i).in.bits.c.exponent := 0.S
    submodules(i).in.bits.c.sign     := 0.U
    submodules(i).in.bits.fcsr       := Seq.fill(8)(0.U(1.W))
    submodules(i).in.bits.op         := FPUOpcode.NULL
    submodules(i).in.valid           := 0.B
    submodules(i).in.bits.fflags     := 0.U
    submodules(i).out.ready          := 0.B
  }


   // Pass the values to the execution units if
   // - Division, square root, multiplication, and fused operations are not in progress
   // - Multiplication or fused operation is in progress and the input operation is multiplication or fused operation
   // - The input operation is division or square root

   // Pass stall control bits
   io.out.bits.stall <> io.in.bits.stall

   when((io.in.bits.stall.stall && io.in.bits.stall.pass) ||
        (~io.in.bits.stall.stall && io.in.bits.stall.pass)) {
     io.out.bits.stall.pass := 0.B
     when(io.in.bits.control.adder) { // Addition / Subtraction
       executorAdd.io.in.bits.a      <> io.in.bits.a
       executorAdd.io.in.bits.b      <> io.in.bits.b
       executorAdd.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorAdd.io.in.bits.op     := io.in.bits.op
       executorAdd.io.in.bits.fcsr   := io.in.bits.fcsr
       executorAdd.io.in.bits.fflags := io.in.bits.fflags
       executorAdd.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.multiplier || io.in.bits.control.fused){ // Multiplication or fused multiply addition/subtraction
       executorMul.io.in.bits.a      <> io.in.bits.a
       executorMul.io.in.bits.b      <> io.in.bits.b
       executorMul.io.in.bits.c      <> io.in.bits.c // UNUSED IN MULTIPLICATION
       executorMul.io.in.bits.op     := io.in.bits.op
       executorMul.io.in.bits.fcsr   := io.in.bits.fcsr
       executorMul.io.in.bits.fflags := io.in.bits.fflags
       executorMul.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.divider) { // Division
       executorDiv.io.in.bits.a      <> io.in.bits.a
       executorDiv.io.in.bits.b      <> io.in.bits.b
       executorDiv.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorDiv.io.in.bits.op     := io.in.bits.op
       executorDiv.io.in.bits.fcsr   := io.in.bits.fcsr
       executorDiv.io.in.bits.fflags := io.in.bits.fflags
       executorDiv.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.comparator) { // Comparison
       executorComp.io.in.bits.a      <> io.in.bits.a
       executorComp.io.in.bits.b      <> io.in.bits.b
       executorComp.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorComp.io.in.bits.op     := io.in.bits.op
       executorComp.io.in.bits.fcsr   := io.in.bits.fcsr
       executorComp.io.in.bits.fflags := io.in.bits.fflags
       executorComp.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.squareRoot) { // Square Root
       executorSqrt.io.in.bits.a      <> io.in.bits.a
       executorSqrt.io.in.bits.b      <> io.in.bits.b // UNUSED
       executorSqrt.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorSqrt.io.in.bits.op     := io.in.bits.op
       executorSqrt.io.in.bits.fcsr   := io.in.bits.fcsr
       executorSqrt.io.in.bits.fflags := io.in.bits.fflags
       executorSqrt.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.bitInject){  // Sign-injection
       executorSgn.io.in.bits.a      <> io.in.bits.a
       executorSgn.io.in.bits.b      <> io.in.bits.b // UNUSED
       executorSgn.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorSgn.io.in.bits.op     := io.in.bits.op
       executorSgn.io.in.bits.fcsr   := io.in.bits.fcsr
       executorSgn.io.in.bits.fflags := io.in.bits.fflags
       executorSgn.io.in.valid       := io.in.valid
     } .elsewhen(io.in.bits.control.classify){
       executorClass.io.in.bits.a      <> io.in.bits.a
       executorClass.io.in.bits.b      <> io.in.bits.b // UNUSED
       executorClass.io.in.bits.c      <> io.in.bits.c // UNUSED
       executorClass.io.in.bits.op     := io.in.bits.op
       executorClass.io.in.bits.fcsr   := io.in.bits.fcsr
       executorClass.io.in.bits.fflags := io.in.bits.fflags
       executorClass.io.in.valid       := io.in.valid
     } .otherwise { // NULL, (U)Int to float, and float to (U)Int
       passthrough.io.in.bits.a      <> io.in.bits.a
       passthrough.io.in.bits.b      <> io.in.bits.b // UNUSED
       passthrough.io.in.bits.c      <> io.in.bits.c // UNUSED
       passthrough.io.in.bits.op     := io.in.bits.op
       passthrough.io.in.bits.fcsr   := io.in.bits.fcsr
       passthrough.io.in.valid       := io.in.valid
       passthrough.io.in.bits.fflags := io.in.bits.fflags
     }
  }


  // Submodule outputs
  val submoduleOut = VecInit(Seq(executorAdd.io.out,
    				 executorMul.io.out,
    				 executorDiv.io.out,
    				 executorSqrt.io.out,
    				 executorComp.io.out,
    				 executorSgn.io.out,
    				 executorClass.io.out,
    				 passthrough.io.out
  				))

  // Connect the ready signals
  // io.in.ready <- Execution unit
  
  val inProc = executorMul.io.out.bits.mulInProc || executorDiv.io.out.bits.divInProc || executorSqrt.io.out.bits.sqrtInProc

  io.in.ready := (~io.in.bits.stall.stall && ~inProc) || ~io.in.valid 

  // Execution unit <- io.out.ready
  for (i <- 0 until submoduleOut.length){
      submoduleOut(i).ready := io.out.ready
  }


  // Outputs
  io.out.bits.result       := Mux1H(submoduleOut.map(_.valid), submoduleOut.map(_.bits.result))
  io.out.bits.fcsr         := Mux1H(submoduleOut.map(_.valid), submoduleOut.map(_.bits.fcsr))
  io.out.bits.fflags       := Mux1H(submoduleOut.map(_.valid), submoduleOut.map(_.bits.fflags))
  io.out.bits.op           := Mux1H(submoduleOut.map(_.valid), submoduleOut.map(_.bits.op))
  io.out.valid 		   := submoduleOut.map(_.valid).reduce(_||_)
  io.out.bits.stall.inProc := inProc

}
