// SPDX-License-Identifier: Apache-2.0

// Initially written by:
// Misbah Memon (misbah.memon@aalto.fi)
// Nooa Jermilä (nooa.jermila@aalto.fi)
// 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


// SINGLE-PRECISION FLOATING-POINT MULTIPLIER + (FUSED OPERATION)
class SPFPMul extends FPUOp {

  // OPERATION
  // 1. Store the exponents and the mantissae in
  //    temorary variables
  // 2. Add the exponents and multiply the mantissae

 // Register retiming
  class mulRegIO extends Bundle {
    val mantissaA  = UInt(26.W)
    val mantissaB  = UInt(26.W)
    val mantissaC  = UInt(26.W)
    val exponentA  = SInt(8.W)
    val exponentB  = SInt(8.W)
    val exponentC  = SInt(8.W)
    val signBitA   = UInt(1.W)
    val signBitB   = UInt(1.W)
    val signBitC   = UInt(1.W)
    val faultFlags = UInt(12.W)
    val status     = Vec(8, UInt(1.W))
    val operation  = FPUOpcode()
    val valid      = Bool()
  }

  val mulReg = RegInit(0.U.asTypeOf(new mulRegIO))

  mulReg.mantissaA  := io.in.bits.a.mantissa
  mulReg.mantissaB  := io.in.bits.b.mantissa
  mulReg.mantissaC  := io.in.bits.c.mantissa
  mulReg.exponentA  := io.in.bits.a.exponent
  mulReg.exponentB  := io.in.bits.b.exponent
  mulReg.exponentC  := io.in.bits.c.exponent
  mulReg.signBitA   := io.in.bits.a.sign
  mulReg.signBitB   := io.in.bits.b.sign
  mulReg.signBitC   := io.in.bits.c.sign
  mulReg.faultFlags := io.in.bits.fflags
  mulReg.status     := io.in.bits.fcsr
  mulReg.operation  := io.in.bits.op
  mulReg.valid      := io.in.valid

  val mantA  = mulReg.mantissaA
  val mantB  = mulReg.mantissaB
  val expA   = mulReg.exponentA
  val expB   = mulReg.exponentB
  val signA  = mulReg.signBitA
  val signB  = mulReg.signBitB
  val fflags = mulReg.faultFlags

  val aNaN  = fflags(3) && fflags(0)
  val bNaN  = fflags(4) && fflags(1)
  val aInf  = fflags(3) && fflags(9)
  val bInf  = fflags(4) && fflags(10)
  val aZero = fflags(6) && fflags(9)
  val bZero = fflags(7) && fflags(10)

  val resultMant = WireDefault(0.U(26.W))
  val resultExp  = WireDefault(0.S(8.W))
  val resultSign = WireDefault(0.U(1.W))
  resultSign := signA ^ signB


  var ff = WireDefault(VecInit(Seq.fill(8)(0.U(1.W))))
  ff := mulReg.status
   
  when(aNaN && bNaN) { // check operand A and operand B
    when(~mantA(22) || ~mantB(22)) {
      // sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // invalid
    }
    resultMant := Cat(1.U, mantA(21,0))
    resultExp  := 128.S
    resultSign := signA
  } .elsewhen(aNaN) { // check operand A
    when (~mantA(22)) {
      // sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // invalid
    }
    resultMant := Cat(1.U, mantA(21,0))
    resultExp  := 128.S
    resultSign := signA
  } .elsewhen(bNaN) { // check operand B
    when (~mantB(22)) {
      // sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // invalid
    }
    resultMant := Cat(1.U, mantB(21,0))
    resultExp  := 128.S
    resultSign := signB
  } .elsewhen(aZero || bZero) {
    // check zero
    when(aInf || bInf) {
      // check zero * Inf
      resultMant := 4194304.U
      resultExp  := 128.S  // output cNaN
      resultSign := 0.U
      ff(FaultFlag.NV.asUInt())   := 1.U
    } .otherwise {
      // check zero * number
      resultMant := 0.U
      resultExp  := -126.S
    }
  } .elsewhen(aInf || bInf) {
    // check operand A or operand B === Inf   
    resultMant := 0.U 
    resultExp  := 128.S
  } .elsewhen(fflags(6) && mantA === 8388608.U) {
    // check if A is 1
    resultMant := mantB
    resultExp  := expB
  } .elsewhen(fflags(7) && mantB === 8388608.U) {
    // check if B is 1
    resultMant := mantA
    resultExp  := expA
  } .otherwise {
    val exponent = expA + expB
    val mantissa = mantA * mantB
    resultMant := mantissa(51, 23)
    resultExp  := exponent
  }
  
  val executorAdd = Module (new SPFPAdd)
  executorAdd.io.in.bits.a.mantissa := 0.U
  executorAdd.io.in.bits.a.exponent := 0.S
  executorAdd.io.in.bits.a.sign     := 0.U
  executorAdd.io.in.bits.b.mantissa := 0.U
  executorAdd.io.in.bits.b.exponent := 0.S
  executorAdd.io.in.bits.b.sign     := 0.U
  executorAdd.io.in.bits.c.mantissa := 0.U
  executorAdd.io.in.bits.c.exponent := 0.S
  executorAdd.io.in.bits.c.sign     := 0.U
  executorAdd.io.in.bits.fcsr       := Seq.fill(8)(0.U(1.W))
  executorAdd.io.in.bits.op         := FPUOpcode.NULL
  executorAdd.io.in.valid           := 0.B
  executorAdd.io.in.bits.fflags     := 0.U
  executorAdd.io.out.ready          := 0.B
  
  val fusedReg = RegInit(0.U.asTypeOf(new mulRegIO))
  fusedReg.mantissaA  := resultMant
  fusedReg.mantissaB  := mulReg.mantissaC
  fusedReg.exponentA  := resultExp
  fusedReg.exponentB  := mulReg.exponentC
  fusedReg.signBitA   := resultSign
  fusedReg.signBitB   := mulReg.signBitC
  fusedReg.faultFlags := mulReg.faultFlags
  fusedReg.status     := ff
  fusedReg.operation  := mulReg.operation
  fusedReg.valid      := mulReg.valid

  //FUSED OPERATIONS
  
  executorAdd.io.in.bits.a.mantissa := fusedReg.mantissaA
  executorAdd.io.in.bits.a.exponent := fusedReg.exponentA
  executorAdd.io.in.bits.a.sign     := fusedReg.signBitA
  executorAdd.io.in.bits.b.mantissa := fusedReg.mantissaB
  executorAdd.io.in.bits.b.exponent := fusedReg.exponentB
  executorAdd.io.in.bits.b.sign     := fusedReg.signBitB
  executorAdd.io.in.bits.fcsr       := fusedReg.status
  executorAdd.io.in.bits.op         := fusedReg.operation
  executorAdd.io.in.valid           := fusedReg.valid
  executorAdd.io.in.bits.fflags     := fusedReg.faultFlags
  executorAdd.io.out.ready          := io.out.ready

  val fused = (fusedReg.operation === FPUOpcode.FMADD_S) ||
	      (fusedReg.operation === FPUOpcode.FMSUB_S) ||
	      (fusedReg.operation === FPUOpcode.FNMADD_S) ||
	      (fusedReg.operation === FPUOpcode.FNMSUB_S)

  // OUTPUTS
  when(!fused) {
    io.out.bits.result.mantissa := fusedReg.mantissaA
    io.out.bits.result.exponent := fusedReg.exponentA
    io.out.bits.result.sign	:= fusedReg.signBitA
    io.out.bits.fcsr 		:= fusedReg.status
    io.out.bits.op 		:= fusedReg.operation
    io.in.ready 		:= io.out.ready
    io.out.valid 		:= fusedReg.valid
    io.out.bits.fflags 		:= fusedReg.faultFlags
  }.otherwise {
    io.out.bits.result	  	<> executorAdd.io.out.bits.result
    io.out.bits.fcsr 	  	:= executorAdd.io.out.bits.fcsr
    io.out.bits.op   	  	:= executorAdd.io.out.bits.op
    io.in.ready		  	:= executorAdd.io.in.ready
    io.out.valid     	  	:= executorAdd.io.out.valid
    io.out.bits.fflags 	  	:= executorAdd.io.out.bits.fflags
  }
  
  io.out.bits.mulInProc  := mulReg.valid || fusedReg.valid
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := 0.B // NOT USED

}

// TEST
class SPFPMulTest extends Module {
  val mul = Module(new SPFPMul)
  val io = IO(new Bundle {
    val in = Input(chiselTypeOf(mul.io.in))
    val out = Output(chiselTypeOf(mul.io.out))
  })
  // Input
  mul.io.in.bits <> io.in.bits
  mul.io.in.valid := io.in.valid
  // Output
  io.out.ready := 1.B
  mul.io.out.ready := io.out.ready
  io.out.bits <> mul.io.out.bits
  io.out.valid <> mul.io.out.valid

}

