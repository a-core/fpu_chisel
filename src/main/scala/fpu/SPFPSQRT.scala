// SPDX-License-Identifier: Apache-2.0

package fpu
import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._

// Single- precision floating point unit
class SPFPSQRT extends FPUOp {

  /** SQRT Operation
   - check if the exponent is even or odd.
        - exponent is even, divide the exponent by 2 and take the square root of mantissa
        - exponent is odd, divide the  (exponent -1) by 2 and take the square root of 2*mantissa


     Overall Steps:
     1. set
        x(0) = operA + 0.25
        y(0) = operA - 0.25
       
     2. Calculate 
         x(i+1) = x(i) + y(i)*d(i)*2^(-i)
         y(i+1) = y(i) + x(i)*d(i)*2^(-i)

         where d(i) is +1 if y(i) < 0 otherwise d(i) = -1

     3. multiply by cordic gain constant A(n)
        finally, sqrt(operA) = x(n)/A(n) 


  */

  val mantA  = io.in.bits.a.mantissa
  val expA   = io.in.bits.a.exponent
  val signA  = io.in.bits.a.sign
  val ff     = WireDefault(VecInit(Seq.fill(8)(0.U(1.W))))
  val fflags = io.in.bits.fflags
  val op     = io.in.bits.op
  
  // register bundle added for non-iterative numbers and exceptions
  class noLoopIO extends Bundle {
    val mantissa = UInt(26.W)
    val exponent = SInt(8.W)
    val sign     = UInt(1.W)
    val valid    = Bool()
  }
 
  val regCount = 27
 
  // register bundle added for iterative numbers
  class loopIO extends Bundle {
    val nX = SInt(extMantLen.W)
    val nY = SInt(extMantLen.W)
    val valid = Bool()
    val sqrtExp = UInt(8.W)
  }

  val opReg = RegInit(VecInit(Seq.fill(regCount)(0.U.asTypeOf(io.in.bits.op))))
  opReg(0) := io.in.bits.op
  for (i <- 0 to regCount - 2){
    opReg(i+1) := opReg(i)
  }

  
  // mantissa is prefixed with three bits
  // overflow bit, sign bit and hidden bit
  // hidden bit is set to 1 
  val mantissa = Cat(1.U(3.W), mantA(22,0))

    // number of cordic iterations    
  val iterations = 25

    // extra bits added for increased accuracy
  val extraBits  = 12
    
  
   // checks the number of times an interation need to be repeated
  val repeats = {
    var times = 0
    var k = 4
    for (i <- 1 to iterations) {
      if (i == k) {
        k = 3*k + 1
        times += 1
      }
    } 
    times
  }

  val totalIterations = iterations + repeats
  val mantLen = 23
    // prefix bits are hidden bit, sign bit and overflow bit in order (LSB to MSB)
  val prefixLen = 3 
    // mantissa length extended for accuracy
  val extMantLen = prefixLen + mantLen + extraBits

  var k = 4    

    // iteration variables, store the intermediate results
    // calculations performed with signed integers only
  var nX = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
  var nY = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
  var tmpX = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
  var tmpY = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
  var extMantissa = Cat(mantissa, 0.U(extraBits.W))
  var nXReg = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
  var nYReg = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))

  var sqrtMant = WireDefault(0.U(mantLen.W))
  var sqrtExp  = WireDefault(0.U(8.W))

  val loopReg = RegInit(VecInit(Seq.fill(iterations+repeats)(0.U.asTypeOf(new loopIO))))
  val noLoopReg = RegInit(VecInit(Seq.fill(regCount)(0.U.asTypeOf(new noLoopIO))))

    // initial value of 0.25 
  val init_val = scala.math.pow(2, extMantLen-prefixLen-2).toLong

    // calculates the inverse cordic gain
    // i = 1,2,3,4,4,5,6,7,8,9,10,11,12,13,13,14...25
  val inv_An = {
    var Ares = 1.0
    var k = 4
    for(i <- 1 to iterations) {
      Ares = Ares * scala.math.sqrt(1.0-scala.math.pow(2, -2*i))
      if (i == k) {
        Ares = Ares * scala.math.sqrt(1.0-scala.math.pow(2, -2*i))
        k = 3*k + 1     
      }
    }
    (1.0/Ares * scala.math.pow(2, extMantLen-prefixLen)).round 
  }


  val zeroExp  = fflags(6)
  val zeroMant = fflags(9)
  val maxExp   = (expA.asUInt === 255.U)

  /** 
   Exceptions:
   - Invalid flag
     - if A is negative
        - if A === 0  -> sqrt = -0
        - if A =/= 0  -> sqrt = cNaN
     - if A is positive
        - A === 0 -> sqrt = 0 (does not raise INV flag)
        - A === sNaN -> sqrt = sNaN (asserts INV flag)
        - A === qNaN -> sqrt = cNaN (does not raise INV flag)
    */
    
  // checks if operand A is negative
  when(signA === 1.U) {
    when(expA === 0.S && zeroMant) {
      // -0
      // sqrt is -0 "h80000000"
      noLoopReg(0).mantissa := 0.U 
      noLoopReg(0).exponent := 0.S
      noLoopReg(0).sign     := 1.U
      noLoopReg(0).valid    := io.in.valid

      for (i <- 0 to regCount -2) {
        noLoopReg(i+1) <> noLoopReg(i)
      }
            
      io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
      io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
      io.out.bits.result.sign     := noLoopReg(regCount-1).sign
    } .otherwise {
      // rs1 < 0
      // sqrt is cNaN "h7fc00000"
      // exp should be -128 if chisel uses 2's complement
      noLoopReg(0).mantissa := 4194304.U 
      noLoopReg(0).exponent := (255.U).asSInt
      noLoopReg(0).sign     := 0.U
      noLoopReg(0).valid    := io.in.valid

      for (i <- 0 to regCount -2) {
        noLoopReg(i+1) <> noLoopReg(i)
      }
            
      io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
      io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
      io.out.bits.result.sign     := noLoopReg(regCount-1).sign
    } 
    ff(FaultFlag.NV.asUInt()) := 1.U // invalid operation
  } .elsewhen(zeroMant && expA === 0.S) {
    // rs1 === +0
    // sqrt is +0
    noLoopReg(0).mantissa := 0.U 
    noLoopReg(0).exponent := 0.S
    noLoopReg(0).sign     := 0.U
    noLoopReg(0).valid    := io.in.valid

    for (i <- 0 to regCount -2) {
      noLoopReg(i+1) <> noLoopReg(i)
    }
            
    io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
    io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
    io.out.bits.result.sign     := noLoopReg(regCount-1).sign
  } .elsewhen(maxExp) {
    when(~zeroMant) {
      when(~mantA(22)) {
        // sNaN  
        // sqrt is "h7fa00000"
        noLoopReg(0).mantissa := 2097152.U 
        noLoopReg(0).exponent := (255.U).asSInt
        noLoopReg(0).sign     := 0.U
        noLoopReg(0).valid    := io.in.valid

        for (i <- 0 to regCount -2) {
          noLoopReg(i+1) <> noLoopReg(i)
        }
            
        io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
        io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
        io.out.bits.result.sign     := noLoopReg(regCount-1).sign

        ff(FaultFlag.NV.asUInt())   := 1.U // invalid operation
      } .otherwise {
        // qNaN
        // sqrt is cNaN
        noLoopReg(0).mantissa := 4194304.U 
        noLoopReg(0).exponent := (255.U).asSInt
        noLoopReg(0).sign     := 0.U
        noLoopReg(0).valid    := io.in.valid

        for (i <- 0 to regCount -2) {
          noLoopReg(i+1) <> noLoopReg(i)
        }
            
        io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
        io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
        io.out.bits.result.sign     := noLoopReg(regCount-1).sign
      }
    } .otherwise {
      // rs1 === Inf
      // sqrt is "h7f800000"
      noLoopReg(0).mantissa := 0.U 
      noLoopReg(0).exponent := (255.U).asSInt
      noLoopReg(0).sign     := 0.U
      noLoopReg(0).valid    := io.in.valid

      for (i <- 0 to regCount -2) {
        noLoopReg(i+1) <> noLoopReg(i)
      }
            
      io.out.bits.result.mantissa := noLoopReg(regCount-1).mantissa
      io.out.bits.result.exponent := noLoopReg(regCount-1).exponent
      io.out.bits.result.sign     := noLoopReg(regCount-1).sign
    }
  } .otherwise {
    

   // checks if the exponent is even 
    when( expA.asUInt % 2.U === 0.U) {
     // when biased exponent is even
     // exponent and mantissa are divided by 2 
     when(expA.asUInt >= 128.U) {
        sqrtExp := 128.U + (( expA.asUInt - 128.U) >> 1.U)
      } .otherwise {
        sqrtExp := 128.U - ((128.U - expA.asUInt) >> 1.U)
      }
      nX(0) := (extMantissa >> 1.U).asSInt()  + init_val.S(extMantLen.W)
      nY(0) := (extMantissa >> 1.U).asSInt()  - init_val.S(extMantLen.W)
    } .otherwise {
      // when biased exponent is odd
      // only exponent is divided by 2
      when(expA.asUInt >= 127.U) {
        sqrtExp := 127.U + ((expA.asUInt - 127.U) >> 1.U)
      } .otherwise {
        sqrtExp := 127.U - ((127.U - expA.asUInt) >> 1.U)
      }
      nX(0) := extMantissa.asSInt() + init_val.S(extMantLen.W)
      nY(0) := extMantissa.asSInt() - init_val.S(extMantLen.W)
    }
  }
     
    // keeps track of how many times the initial value must be bit shifted
    var shift = 1
     
    loopReg(0).nX := nX(0)
    loopReg(0).nY := nY(0)
    loopReg(0).valid := io.in.valid
    loopReg(0).sqrtExp := sqrtExp

    nXReg(0) := loopReg(0).nX 
    nYReg(0) := loopReg(0).nY      

    for( i <- 1 until iterations + repeats) {
      
      tmpX(i-1)  := nXReg(i-1) >> shift.U
      tmpY(i-1)  := nYReg(i-1) >> shift.U

      if (i == k) {
        k = 3*k + 1
      } else {
        shift += 1
      }
        
      when ( nYReg(i-1)(extMantLen-1) === 1.U) {
        // y is less than zero
        nX(i) := nXReg(i-1) + tmpY(i-1)
        nY(i) := nYReg(i-1) + tmpX(i-1)
        loopReg(i).nX := nX(i)
        loopReg(i).nY := nY(i)
        nXReg(i) := loopReg(i).nX
        nYReg(i) := loopReg(i).nY  
      } .otherwise {
        // y is more than zero
        nX(i) := nXReg(i-1) - tmpY(i-1)
        nY(i) := nYReg(i-1) - tmpX(i-1)
        loopReg(i).nX := nX(i)
        loopReg(i).nY := nY(i)
        nXReg(i) := loopReg(i).nX
        nYReg(i) := loopReg(i).nY  
      }
      loopReg(i).valid := loopReg(i-1).valid
      loopReg(i).sqrtExp := loopReg(i-1).sqrtExp
    }
  
  val inv_An2_hw = WireDefault(inv_An.S(extMantLen.W))

  // multiply by inverse coordic gain
  // this has a length of 2*extManLen
  // with binary point at 2*mantLen + 2*extraBits -1
  val mulRes = inv_An2_hw * nXReg(totalIterations-1)

  // truncates the extra bits except for three G. R and an extra bit
  // the remaining value has a length of mantLen+prefixLen+3
  val tempMant = (mulRes >> ( 2*extraBits + mantLen -3))(mantLen+prefixLen+2,0)

  // generates the sticky bit
  // which is 1 if any of the bits after the R bit is 1
  val sticky = mulRes(2*extraBits+mantLen -3,0).orR

  var finalMant = WireDefault(0.U((mantLen+prefixLen+2).W))
  var finalExp = WireDefault(0.U(8.W))
  var finalExtMant = WireDefault(0.U((mantLen+prefixLen).W))

  when(tempMant(mantLen+3) === 0.U) {
    // mantissa less than 1, multiply by 2
    //finalExtMant:= finalMant(mantLen+prefixLen+2,1)
    finalMant := (tempMant << 1)(mantLen+prefixLen+2,1)
    finalExp  := (loopReg(iterations+repeats -1).sqrtExp.asSInt() - 1.S).asUInt
  } .otherwise {
    // mantissa more than 1
    finalMant := tempMant(mantLen+prefixLen+2,1)
    finalExp  := loopReg(iterations+repeats -1).sqrtExp
  }

  io.out.bits.result.mantissa := Cat(finalMant(mantLen+prefixLen,0), sticky)(25,3)
  io.out.bits.result.exponent := finalExp.asSInt
  io.out.bits.result.sign     := 0.U

  io.out.bits.op := opReg(regCount-1)
  io.out.bits.fcsr := ff
  io.out.bits.fflags := io.in.bits.fflags
  io.out.valid := loopReg(iterations+repeats-1).valid
  io.in.ready := io.out.ready

  io.out.bits.mulInProc  := 0.B // NOT USED
  io.out.bits.divInProc  := 0.B // NOT USED
  io.out.bits.sqrtInProc := loopReg.map(_.valid).reduce(_ || _)

}


// SQUARE ROOT TEST
class SPFPSQRTTest extends Module {
    val sqrt = Module(new SPFPSQRT)
    val io = IO(new Bundle {
        val in = Input(chiselTypeOf(sqrt.io.in))
        val out = Output(chiselTypeOf(sqrt.io.out))
    })
    // Input
    //io.in.ready := sqrt.io.in.ready
    sqrt.io.in.bits <> io.in.bits
    sqrt.io.in.valid := io.in.valid
    // Output
    io.out.ready := 1.B
    sqrt.io.out.ready := io.out.ready
    io.out.bits <>  sqrt.io.out.bits
    io.out.valid := sqrt.io.out.valid

}
