 // SPDX-License-Identifier: Apache-2.0

// Initially written by:
// Misbah Memon (misbah.memon@aalto.fi)
// Nooa Jermilä (nooa.jermila@aalto.fi)
// 2024-08-24

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._


// SINGLE-PRECISION FLOATING-POINT DIVIDER
class SPFPDiv extends FPUOp {

    /*
    1. check for special cases
    2. get sign with XOR
    3. get exponent with sum of exponents
    4. get mantissa with div of mantissas
    5. compose result
    */

  // Register retiming
  class divRegIO extends Bundle { // DIV : A / B = C
    val mantissaA  = UInt(26.W)  // Operand A mantissa
    val mantissaB  = UInt(26.W)  // Operand B mantissa
    val mantissaC  = UInt(26.W)  // result mantissa
    val exponentA  = SInt(8.W)
    val exponentB  = SInt(8.W)
    val exponentC  = SInt(8.W)
    val signBitA   = UInt(1.W)
    val signBitB   = UInt(1.W)
    val signBitC   = UInt(1.W)
    val faultFlags = UInt(12.W)
    val status     = Vec(8, UInt(1.W))
    val op         = FPUOpcode()
    val valid      = Bool()
    val exMantA    = UInt(26.W)
  }

  val divReg = RegInit(VecInit(Seq.fill(26)(0.U.asTypeOf(new divRegIO))))

  divReg(0).mantissaA  := io.in.bits.a.mantissa
  divReg(0).mantissaB  := io.in.bits.b.mantissa
  divReg(0).mantissaC  := 0.U
  divReg(0).exponentA  := io.in.bits.a.exponent
  divReg(0).exponentB  := io.in.bits.b.exponent
  divReg(0).exponentC  := 0.S
  divReg(0).signBitA   := io.in.bits.a.sign
  divReg(0).signBitB   := io.in.bits.b.sign
  divReg(0).signBitC   := 0.U
  divReg(0).faultFlags := io.in.bits.fflags
  divReg(0).status     := io.in.bits.fcsr
  divReg(0).op         := io.in.bits.op
  divReg(0).valid      := io.in.valid
  divReg(0).exMantA    := io.in.bits.a.mantissa

  // Division

  for (i <- 0 to 24) {
    divReg(i+1) <> divReg(i)
    when((Cat(0.U(1.W), divReg(i).mantissaA).asSInt - Cat(0.U(1.W), divReg(i).mantissaB).asSInt) < 0.S) {
      divReg(i+1).mantissaA := divReg(i).mantissaA << 1.U
      divReg(i+1).mantissaC := divReg(i).mantissaC << 1.U
    }. otherwise {
      divReg(i+1).mantissaA := (divReg(i).mantissaA +& (~divReg(i).mantissaB + 1.U)) << 1.U
      divReg(i+1).mantissaC := Cat(divReg(i).mantissaC(24, 0), 1.U(1.W))
    }
  }
  
  divReg(25).exponentC := divReg(24).exponentA - divReg(24).exponentB - 1.S
  divReg(25).signBitC  := divReg(24).signBitA ^ divReg(24).signBitB
 
  val mantA  = divReg(25).exMantA
  val mantB  = divReg(25).mantissaB
  val expA   = divReg(25).exponentA
  val expB   = divReg(25).exponentB
  val signA  = divReg(25).signBitA
  val signB  = divReg(25).signBitB
  val ff     = WireInit(divReg(25).status)
  val fflags = divReg(25).faultFlags
  val op     = divReg(25).op

  val aNaN  = fflags(3) && fflags(0)
  val bNaN  = fflags(4) && fflags(1)
  val zeroA = fflags(6) && fflags(9)
  val zeroB = fflags(7) && fflags(10)
  val infA  = fflags(3) && fflags(9) 
  val infB  = fflags(4) && fflags(10)

  when (aNaN && bNaN) {
    // check operand A & B
    when (~mantA(22) || ~mantB(22)) {
      //sNaN
      ff(FaultFlag.NV.asUInt()) := 1.U // Invalid
    }
    io.out.bits.result.mantissa := Cat(1.U(4.W), mantA(21,0))
    io.out.bits.result.exponent := 128.S
    io.out.bits.result.sign     := signA   
  } .elsewhen(aNaN) {
    // check operand A
    when (~mantA(22)) {
      //sNaN
      ff(FaultFlag.NV.asUInt) := 1.U // Invalid
    }
    io.out.bits.result.mantissa := Cat(1.U(4.W), mantA(21,0))
    io.out.bits.result.exponent := 128.S
    io.out.bits.result.sign     := signA
  } .elsewhen(bNaN) {
    // check operand B
    when(~mantB(22)) {
      // sNaN
      ff(FaultFlag.NV.asUInt) := 1.U // Invalid
    }
    io.out.bits.result.mantissa := Cat(1.U(4.W), mantB(21,0))
    io.out.bits.result.exponent := 128.S
    io.out.bits.result.sign     := signB 
  } .elsewhen(zeroA || zeroB ){
    when(zeroA && zeroB){
      // 0/0 ==> illegal === qNaN
      io.out.bits.result.mantissa := 4194304.U
      io.out.bits.result.exponent := 128.S
      io.out.bits.result.sign     := 1.U
      ff(FaultFlag.NV.asUInt)     := 1.U  // Invalid
    } .elsewhen(zeroA && ~zeroB){
      // 0/x => +/- 0
      io.out.bits.result.mantissa := mantA
      io.out.bits.result.exponent := expA
      io.out.bits.result.sign     := signA ^ signB
    } .otherwise {
      // x/0 => division by zero
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := 128.S
      io.out.bits.result.sign     := signA ^ signB
      when(~infA){
        // inf/0 => inf
        ff(FaultFlag.DZ.asUInt()) := 1.U  // Division by zero
      }
    } 
  } .elsewhen(infA || infB){
    // check infinities
    when(~infA  && infB){
      // x/Inf => zero 
      io.out.bits.result.mantissa := 0.U
      io.out.bits.result.exponent := -126.S
      io.out.bits.result.sign     := signA ^ signB
    } .elsewhen(infA && ~infB) {
      // inf/x => Inf
      io.out.bits.result.mantissa := mantA
      io.out.bits.result.exponent := expA
      io.out.bits.result.sign     := signA ^ signB
    } .otherwise {
       // inf/inf => illegal === qNaN
      io.out.bits.result.mantissa := 4194304.U
      io.out.bits.result.exponent := 128.S
      io.out.bits.result.sign     := 1.U
      ff(FaultFlag.NV.asUInt())   := 1.U  // Invalid
    }
  } .elsewhen(zeroB) {
    // check B = 1 
    io.out.bits.result.mantissa := mantA
    io.out.bits.result.exponent := expA
    io.out.bits.result.sign     := signA ^ signB
  }. otherwise {
    io.out.bits.result.mantissa := divReg(25).mantissaC
    io.out.bits.result.exponent := divReg(25).exponentC
    io.out.bits.result.sign     := divReg(25).signBitC
  }

  io.out.bits.fcsr	      := divReg(25).status
  io.out.bits.op              := divReg(25).op
  io.out.bits.fflags	      := divReg(25).faultFlags
  io.out.valid                := divReg(25).valid
  io.in.ready                 := io.out.ready
  io.out.bits.mulInProc       := 0.B // NOT USED
  io.out.bits.divInProc       := divReg.map(_.valid).reduce(_||_)
  io.out.bits.sqrtInProc      := 0.B // NOT USED

}

// TEST
class SPFPDivTest extends Module {
  val div = Module(new SPFPDiv)
  val io = IO(new Bundle {

    val in = Input(chiselTypeOf(div.io.in))
    val out = Output(chiselTypeOf(div.io.out))
  })
  // Input
  div.io.in.bits <> io.in.bits
  div.io.in.valid := io.in.valid
  // Output
  io.out.ready := 1.B
  div.io.out.ready := io.out.ready
  io.out.bits <> div.io.out.bits
  io.out.valid <> div.io.out.valid

}

