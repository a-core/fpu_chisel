// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPCompSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

    it should "Compare the inputs" in {
        test(new SPFPCompTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
        c.io.in.bits.a.mantissa   poke  8388608.U 
        c.io.in.bits.a.exponent   poke  0.S    
        c.io.in.bits.a.sign       poke  0.U
        c.io.in.bits.b.mantissa   poke  8388608.U 
        c.io.in.bits.b.exponent   poke  1.S    
        c.io.in.bits.b.sign       poke  0.U
        c.io.in.bits.op           poke FPUOpcode.FCMP_FEQ_S
        //c.io.in.bits.fcsr         poke 0.U
        c.io.in.valid             poke 1.B
        c.clock.step(10)

        // testing equal-to op
        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FCMP_FEQ_S
        c.io.out.valid                expect 1.B

        // testing less-than-equal-to op
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FCMP_FLE_S
        c.io.out.bits.result.mantissa expect 1.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FCMP_FLE_S
        c.io.out.valid                expect 1.B

        // testing less-than op 
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FCMP_FLT_S
        c.io.out.bits.result.mantissa expect 1.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FCMP_FLT_S
        c.io.out.valid                expect 1.B
 
        // testing max op
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FMAX_S
        c.io.out.bits.result.mantissa expect 8388608.U   
        c.io.out.bits.result.exponent expect 1.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FMAX_S
        c.io.out.valid                expect 1.B
 
        // testing min op 
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FMIN_S
        c.io.out.bits.result.mantissa expect 8388608.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FMIN_S
        c.io.out.valid                expect 1.B
      
        }
    }
}
     
