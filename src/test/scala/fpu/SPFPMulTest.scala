// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPMulSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

  it should "multiply a by b" in {
    test(new SPFPMulTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>

	// Multiplication
    c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 1.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 12582912.U
	c.io.in.bits.b.exponent poke 0.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -127.S
	c.io.in.bits.c.sign poke 0.U
    	c.io.in.bits.op poke FPUOpcode.FMUL_S
    	//c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
    	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 12582912.U
	c.io.out.bits.result.exponent expect 1.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

    }
  }

}

