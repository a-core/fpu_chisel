// SPDX-liscense-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPSQRTSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

    it should "take the square root" in {
        test(new SPFPSQRTTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
/**
        // Test case 1: rs is a negative number
        c.io.in.bits.op           poke FPUOpcode.FSQRT_S
        c.io.in.valid             poke 1.B
        c.io.in.bits.a.mantissa   poke 2.U 
        c.io.in.bits.a.exponent   poke 4.S    
        c.io.in.bits.a.sign       poke 1.U
        //c.io.in.bits.fcsr       poke 0.U
        
        c.clock.step(10)

        c.io.out.bits.result.mantissa expect 4194304.U   
        c.io.out.bits.result.exponent expect -126.S   
        c.io.out.bits.result.sign     expect 0.U  
        c.io.out.bits.op              expect FPUOpcode.FSQRT_S
        c.io.out.valid                expect 1.B
        //c.io.out.bits.fcsr          expect 0.U

        // Test case 1: rs is -0
        c.io.in.bits.op           poke FPUOpcode.FSQRT_S
        c.io.in.valid             poke 1.B
        c.io.in.bits.a.mantissa   poke 0.U 
        c.io.in.bits.a.exponent   poke 0.S    
        c.io.in.bits.a.sign       poke 1.U
        //c.io.in.bits.fcsr       poke 0.U
        
        c.clock.step(10)

        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 1.U  
        c.io.out.bits.op              expect FPUOpcode.FSQRT_S
        c.io.out.valid                expect 1.B
        //c.io.out.bits.fcsr          expect 0.U

        // Test case 3: rs is +0
        c.io.in.bits.op           poke FPUOpcode.FSQRT_S
        c.io.in.valid             poke 1.B
        c.io.in.bits.a.mantissa   poke 0.U 
        c.io.in.bits.a.exponent   poke 0.S    
        c.io.in.bits.a.sign       poke 0.U
        //c.io.in.bits.fcsr       poke 0.U
        
        c.clock.step(10)

        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        c.io.out.bits.op              expect FPUOpcode.FSQRT_S
        c.io.out.valid                expect 1.B
        //c.io.out.bits.fcsr          expect 0.U

        // Test case 4: even exponent number , 0 < rs < inf
        // rs is 4 * 10^2
        // sqrt should be 2 * 10
        c.io.in.bits.op           poke FPUOpcode.FSQRT_S
        c.io.in.valid             poke 1.B
        c.io.in.bits.a.mantissa   poke 8388612.U 
        c.io.in.bits.a.exponent   poke 2.S    
        c.io.in.bits.a.sign       poke 0.U
        //c.io.in.bits.fcsr       poke 0.U
        
        c.clock.step(10)

        c.io.out.bits.result.mantissa expect 63264179.U   
        c.io.out.bits.result.exponent expect 1.S   
        c.io.out.bits.result.sign     expect 0.U  
        c.io.out.bits.op              expect FPUOpcode.FSQRT_S
        c.io.out.valid                expect 1.B
        //c.io.out.bits.fcsr          expect 0.U
*/
     // Test case 5: odd exponent number , 0 < rs < inf
        // rs is 10 * 10^3
        // sqrt should be 100
        c.io.in.bits.op           poke FPUOpcode.FSQRT_S
        c.io.in.valid             poke 1.B
        c.io.in.bits.a.mantissa   poke 8388618.U 
        c.io.in.bits.a.exponent   poke 3.S    
        c.io.in.bits.a.sign       poke 0.U
        //c.io.in.bits.fcsr       poke 0.U
        
        c.clock.step(10)

        c.io.out.bits.result.mantissa expect 18088987.U   
        c.io.out.bits.result.exponent expect 1.S   
        c.io.out.bits.result.sign     expect 0.U  
        c.io.out.bits.op              expect FPUOpcode.FSQRT_S
        c.io.out.valid                expect 1.B
        //c.io.out.bits.fcsr          expect 0.U


      } 
    }
}    
