// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPSGNJSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

    it should "Injects Sign bit" in {
        test(new SPFPSGNJTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
        c.io.in.bits.a.mantissa   poke 8388608.U 
        c.io.in.bits.a.exponent   poke 0.S    
        c.io.in.bits.a.sign       poke 0.U
        c.io.in.bits.b.mantissa   poke 8388608.U 
        c.io.in.bits.b.exponent   poke 1.S    
        c.io.in.bits.b.sign       poke 1.U
        c.io.in.bits.op           poke FPUOpcode.FSGNJ_S
        //c.io.in.bits.fcsr         poke 0.U
        c.io.in.valid             poke 1.B
        c.clock.step(10)

        // testing SGNJ - sign bit is from b
        c.io.out.bits.result.mantissa expect 8388608.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 1.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJ_S
        c.io.out.valid                expect 1.B

        // testing SGNJN - sign bit is ~b
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FSGNJN_S
        c.io.out.bits.result.mantissa expect 8388608.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJN_S
        c.io.out.valid                expect 1.B

        // testing SGNJX - sign bit is XOR  
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FSGNJX_S
        c.io.out.bits.result.mantissa expect 8388608.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 1.U
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJX_S
        c.io.out.valid                expect 1.B

        // testing for different set of inputs
        c.io.in.bits.a.mantissa   poke  0.U 
        c.io.in.bits.a.exponent   poke  0.S    
        c.io.in.bits.a.sign       poke  1.U
        c.io.in.bits.b.mantissa   poke  8388608.U 
        c.io.in.bits.b.exponent   poke  1.S    
        c.io.in.bits.b.sign       poke  1.U
        c.io.in.bits.op           poke FPUOpcode.FSGNJ_S
        //c.io.in.bits.fcsr         poke 0.U
        c.io.in.valid             poke 1.B
        c.clock.step(10)

         // testing SGNJ - sign bit is b
        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 1.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJ_S
        c.io.out.valid                expect 1.B

        // testing SGNJN - sign bit is ~b  
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FSGNJN_S
        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJN_S
        c.io.out.valid                expect 1.B

        // testing SGNJX - sign bit is XOR
        c.clock.step(10)
        c.io.in.bits.op               poke FPUOpcode.FSGNJX_S
        c.io.out.bits.result.mantissa expect 0.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U
        //c.io.out.bits.fcsr            expect 0.U
        c.io.out.bits.op              expect FPUOpcode.FSGNJX_S
        c.io.out.valid                expect 1.B

        }
    }       
}



  
