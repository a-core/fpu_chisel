// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPAddSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

  it should "add values together" in {
    test(new SPFPAddTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
	// Addition
        c.io.in.bits.a.mantissa poke 58720256.U
	c.io.in.bits.a.exponent poke 3.S
	c.io.in.bits.a.sign poke 1.U
	c.io.in.bits.b.mantissa poke 55574528.U
	c.io.in.bits.b.exponent poke 3.S
	c.io.in.bits.b.sign poke 1.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
        c.io.in.bits.op poke FPUOpcode.FSUB_S
        //c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
        c.clock.step(10)
	c.io.out.bits.result.mantissa expect 3145728.U
        //c.io.out.bits.result.mantissa expect 63963136.U
	c.io.out.bits.result.exponent expect 3.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

	c.io.in.bits.a.mantissa poke 0.U
	c.io.in.bits.a.exponent poke -127.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 0.U
	c.io.in.bits.b.exponent poke -127.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -127.S
	c.io.in.bits.c.sign poke 0.U
        c.io.in.bits.op poke FPUOpcode.FADD_S
        //c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
        c.clock.step(10)
	c.io.out.bits.result.mantissa expect 0.U
	c.io.out.bits.result.exponent expect -127.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

        c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 0.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 14680064.U
	c.io.in.bits.b.exponent poke 2.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
        c.io.in.bits.op poke FPUOpcode.FSUB_S
        //c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
        c.clock.step(10)
	c.io.out.bits.result.mantissa expect 12582912.U
	c.io.out.bits.result.exponent expect 2.S
	c.io.out.bits.result.sign expect 1.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

        c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 0.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 10485760.U
	c.io.in.bits.b.exponent poke 3.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
        c.io.in.bits.op poke FPUOpcode.FSUB_S
        //c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
        c.clock.step(10)
	c.io.out.bits.result.mantissa expect 9437184.U
	c.io.out.bits.result.exponent expect 3.S
	c.io.out.bits.result.sign expect 1.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

        c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 0.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 8388608.U
	c.io.in.bits.b.exponent poke 1.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
        c.io.in.bits.op poke FPUOpcode.FSUB_S
        //c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
        c.clock.step(10)
	c.io.out.bits.result.mantissa expect 4194304.U
	c.io.out.bits.result.exponent expect 1.S
	c.io.out.bits.result.sign expect 1.U
	//c.io.out.bits.fcsr expect 0.U
	c.clock.step(1)

	// Subtraction
	c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 1.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 8388608.U
	c.io.in.bits.b.exponent poke 0.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
    	c.io.in.bits.op poke FPUOpcode.FSUB_S
	//c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
    	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 4194304.U
	c.io.out.bits.result.exponent expect 1.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U

	c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 1.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 8388608.U
	c.io.in.bits.b.exponent poke 2.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
    	c.io.in.bits.op poke FPUOpcode.FSUB_S
	//c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
    	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 62914560.U
	c.io.out.bits.result.exponent expect 2.S
	c.io.out.bits.result.sign expect 1.U
	//c.io.out.bits.fcsr expect 0.U

	c.io.in.bits.a.mantissa poke 8388608.U
	c.io.in.bits.a.exponent poke 1.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 8388608.U
	c.io.in.bits.b.exponent poke 1.S
	c.io.in.bits.b.sign poke 0.U
	c.io.in.bits.c.mantissa poke 0.U
	c.io.in.bits.c.exponent poke -126.S
	c.io.in.bits.c.sign poke 0.U
    	c.io.in.bits.op poke FPUOpcode.FSUB_S
	//c.io.in.bits.fcsr poke 0.U
	c.io.in.valid poke 1.B
    	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 0.U
	c.io.out.bits.result.exponent expect 1.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U
    }
  }

}

