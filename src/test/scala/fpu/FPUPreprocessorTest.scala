// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class FPUPreprocessorSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

    it should "Preprocess the inputs" in {
        test(new FPUPreprocessorTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
        c.io.in.bits.rs1 poke floatToIEEEBits(1.0f).U
        c.io.in.bits.rs2 poke floatToIEEEBits(2.0f).U
        c.io.in.bits.rs3 poke floatToIEEEBits(0.0f).U
        c.io.in.bits.op poke FPUOpcode.FADD_S
	c.io.in.bits.fcsr.foreach(_.poke(0.U))
	c.io.in.valid poke 1.B
        c.clock.step(10)
        c.io.out.bits.a.mantissa expect 8388608.U
        c.io.out.bits.a.exponent expect 0.S
        c.io.out.bits.a.sign expect 0.U
        c.io.out.bits.b.mantissa expect 8388608.U
        c.io.out.bits.b.exponent expect 1.S
        c.io.out.bits.b.sign expect 0.U
        c.io.out.bits.c.mantissa expect 0.U
        c.io.out.bits.c.exponent expect -126.S
        c.io.out.bits.c.sign expect 0.U
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
	c.clock.step(10)
	c.io.in.bits.rs1 poke floatToIEEEBits(0.0f).U
	c.io.in.bits.rs2 poke floatToIEEEBits(0.0f).U
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 0.U
        c.io.out.bits.a.exponent expect -126.S
        c.io.out.bits.a.sign expect 0.U
        c.io.out.bits.b.mantissa expect 0.U
        c.io.out.bits.b.exponent expect -126.S
        c.io.out.bits.b.sign expect 0.U
        c.io.out.bits.c.mantissa expect 0.U
        c.io.out.bits.c.exponent expect -126.S
        c.io.out.bits.c.sign expect 0.U
        c.io.out.bits.fcsr.foreach(_.expect(0.U))

	c.io.in.bits.rs1 poke 0.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_W
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 0.U
	c.io.out.bits.a.exponent expect -126.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.op poke FPUOpcode.FCVT_S_WU
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 0.U
	c.io.out.bits.a.exponent expect -126.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 1.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_W
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 8388608.U
	c.io.out.bits.a.exponent expect 0.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 1.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_WU
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 8388608.U
	c.io.out.bits.a.exponent expect 0.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 100.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_W
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 13107200.U
	c.io.out.bits.a.exponent expect 6.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 100.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_WU
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 13107200.U
	c.io.out.bits.a.exponent expect 6.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 0.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_W
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 0.U
	c.io.out.bits.a.exponent expect -126.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 0.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_WU
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 0.U
	c.io.out.bits.a.exponent expect -126.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 20400008.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_W
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 10200004.U
	c.io.out.bits.a.exponent expect 24.S
	c.io.out.bits.a.sign expect 0.U
    
	c.io.in.bits.rs1 poke 20400008.U
	c.io.in.bits.op poke FPUOpcode.FCVT_S_WU
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 10200004.U
	c.io.out.bits.a.exponent expect 24.S
	c.io.out.bits.a.sign expect 0.U

	c.io.in.bits.rs1 poke 4286578688L.U
	c.io.in.bits.op poke FPUOpcode.FADD_S
	c.clock.step(10)
	c.io.out.bits.a.mantissa expect 8388608.U
	c.io.out.bits.a.exponent expect 127.S
	c.io.out.bits.a.sign expect 1.U
        }
    }
}
