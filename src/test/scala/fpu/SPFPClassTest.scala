// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPClassSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

    it should "Classify the source register" in {
        test(new SPFPClassTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>

        c.io.in.bits.op			poke FPUOpcode.FCLASS_S
        c.io.in.bits.fcsr.foreach(_.poke(0.U))
        c.io.in.valid			poke 1.B

        // test case 1 : negative infinity
	c.io.in.bits.rs1		poke 4286578688L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 1.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign     	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 2 : positive infinity
	c.io.in.bits.rs1		poke 2139095040.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 128.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 3 : negative normal number
	c.io.in.bits.rs1		poke 4219469825L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 2.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op              expect FPUOpcode.FCLASS_S
        c.io.out.valid                expect 1.B

        // test case 4 : positive normal number
	c.io.in.bits.rs1		poke 2113929217.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa expect 64.U   
        c.io.out.bits.result.exponent expect 0.S   
        c.io.out.bits.result.sign     expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op              expect FPUOpcode.FCLASS_S
        c.io.out.valid                expect 1.B

        // test case 5 : negative subnormal number
	c.io.in.bits.rs1		poke 2147483649L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 4.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B
        
        // test case 6 : positive subnormal number
	c.io.in.bits.rs1		poke 1.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 32.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 7 : negative zero
	c.io.in.bits.rs1		poke 2147483648L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 8.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 8 : positive zero
	c.io.in.bits.rs1		poke 0.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 16.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B


        // test case 9a : signaling NAN for positive number
	c.io.in.bits.rs1		poke 2139095041.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 256.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 9b : signaling NAN for negative number
	c.io.in.bits.rs1		poke 4286578689L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 256.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        // test case 10a : quite NAN for positive number
        c.io.in.bits.rs1		poke 2134900737.U
        c.io.out.bits.result.mantissa	expect 512.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B


        // test case 10b : quite NAN for negative number
	c.io.in.bits.rs1		poke 4290772996L.U
        c.clock.step(10)
        c.io.out.bits.result.mantissa	expect 512.U   
        c.io.out.bits.result.exponent	expect 0.S   
        c.io.out.bits.result.sign	expect 0.U  
        c.io.out.bits.fcsr.foreach(_.expect(0.U))
        c.io.out.bits.op		expect FPUOpcode.FCLASS_S
        c.io.out.valid			expect 1.B

        }
   }  
}
