// SPDX-License-Identifier: Apache-2.0

package fpu

trait SPFPTestTools {
  /** Defines path for TestFloat tools */
  val testfloat_path = sys.env.get("TESTFLOAT_PATH") match {
    case Some(x) => x
    case None    => "TestFloat-3e/build/Linux-x86_64-GCC"
  }

  /** Convert float to Chisel hex string */
  def floatToIEEEBits(float: Float) : String = {
    val hexStr = Integer.toHexString(java.lang.Float.floatToIntBits(float)).toUpperCase()
    "h" + hexStr
  }

  /** Convert Chisel hex string to float */
  def IEEEBitsToFloat(hexstr: String) : Float = {
    val parsedStr = hexstr.stripPrefix("h").replace("_", "")
    val intValue = Integer.parseInt(parsedStr, 16)
    val floatValue = java.lang.Float.intBitsToFloat(intValue)
    floatValue
  }

}
