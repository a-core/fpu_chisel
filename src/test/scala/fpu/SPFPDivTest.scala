// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class SPFPDivSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {

  it should "divide a by b" in {
    test(new SPFPDivTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>

    // Division
    	c.io.in.bits.op poke FPUOpcode.FDIV_S
    	c.io.in.bits.a.mantissa poke 12582912.U
	c.io.in.bits.a.exponent poke 0.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 8388608.U
	c.io.in.bits.b.exponent poke 1.S
	c.io.in.bits.b.sign poke 0.U
   	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 12582912.U
	c.io.out.bits.result.exponent expect -1.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U

	c.io.in.bits.a.mantissa poke 12582912.U
	c.io.in.bits.a.exponent poke 3.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 12582912.U
	c.io.in.bits.b.exponent poke 1.S
	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 8388608.U
	c.io.out.bits.result.exponent expect 2.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U

	c.io.in.bits.a.mantissa poke 11141120.U
	c.io.in.bits.a.exponent poke 6.S
	c.io.in.bits.a.sign poke 0.U
	c.io.in.bits.b.mantissa poke 10485760.U
	c.io.in.bits.b.exponent poke 3.S
	c.clock.step(10)
	c.io.out.bits.result.mantissa expect 8912896.U
	c.io.out.bits.result.exponent expect 3.S
	c.io.out.bits.result.sign expect 0.U
	//c.io.out.bits.fcsr expect 0.U


    }
  }

}

