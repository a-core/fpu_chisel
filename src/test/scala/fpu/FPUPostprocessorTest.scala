// SPDX-License-Identifier: Apache-2.0

package fpu

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chiseltest.ChiselScalatestTester

class FPUPostprocessorSpec extends AnyFlatSpec with ChiselScalatestTester with SPFPTestTools {
   
   it should "Postprocess the inputs" in {
       test (new FPUPostprocessorTest).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
      
       // FCLASS_S
       c.io.in.bits.result.mantissa  poke    512.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FCLASS_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  512.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       // FMAX_S
       c.io.in.bits.result.mantissa  poke    512.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FMAX_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  947912704.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       // FMIN_S
       c.io.in.bits.result.mantissa  poke    512.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FMIN_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  947912704.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // FCMP_FLT_S
       c.io.in.bits.result.mantissa  poke    1.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FCMP_FLT_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  1.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // FCMP_FEQ_S
       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FCMP_FEQ_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  0.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // FCMP_FLE_S
       c.io.in.bits.result.mantissa  poke    1.U
       c.io.in.bits.result.exponent  poke    0.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FCMP_FLE_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  1.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // FADD_S
       c.io.in.bits.result.mantissa  poke    8388608.U
       c.io.in.bits.result.exponent  poke    -20.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  897581056.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       c.io.in.bits.result.mantissa  poke    12582912.U
       c.io.in.bits.result.exponent  poke    1.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  1077936128.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       c.io.in.bits.result.mantissa  poke    16777216.U
       c.io.in.bits.result.exponent  poke    1.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  1082130432.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    1.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  0.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       // FADD_S
       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    -126.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  0.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       // FADD_S
       c.io.in.bits.result.mantissa  poke    12582912.U
       c.io.in.bits.result.exponent  poke    -126.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FADD_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  12582912.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // SUB_S
       c.io.in.bits.result.mantissa  poke    12582912.U
       c.io.in.bits.result.exponent  poke    2.S  
       c.io.in.bits.result.sign      poke    1.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FSUB_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  3212836864L.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B


       // FSGNJ_S
       c.io.in.bits.result.mantissa  poke    10.U
       c.io.in.bits.result.exponent  poke    -1.S  
       c.io.in.bits.result.sign      poke    0.U   
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op               poke    FPUOpcode.FSGNJ_S
       c.io.in.valid                 poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd              expect  891289600.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid                expect  1.B

       // FCVT_W_S:
       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    -126.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  0.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    -126.S
       c.io.in.bits.result.sign	     poke    1.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  2147483648L.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388611.U
       c.io.in.bits.result.exponent  poke    114.S
       c.io.in.bits.result.sign	     poke    1.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  2147483648L.U
       c.io.out.bits.fcsr(4)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388609.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    12582913.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    16777215.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388608.U
       c.io.in.bits.result.exponent  poke    1.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  2.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388608.U
       c.io.in.bits.result.exponent  poke    127.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_W_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  2147483647L.U
       c.io.out.bits.fcsr(4)         expect  1.U
       c.io.out.valid		     expect  1.U

       // FCVT_WU_S
       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    -126.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  0.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    0.U
       c.io.in.bits.result.exponent  poke    -126.S
       c.io.in.bits.result.sign	     poke    1.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  0.U
       c.io.out.bits.fcsr(4)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388609.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    12582913.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    16777215.U
       c.io.in.bits.result.exponent  poke    0.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  1.U
       c.io.out.bits.fcsr(0)	     expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388608.U
       c.io.in.bits.result.exponent  poke    1.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  2.U
       c.io.out.bits.fcsr.foreach(_.expect(0.U))
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388608.U
       c.io.in.bits.result.exponent  poke    127.S
       c.io.in.bits.result.sign	     poke    0.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  4294967295L.U
       c.io.out.bits.fcsr(4)         expect  1.U
       c.io.out.valid		     expect  1.U

       c.io.in.bits.result.mantissa  poke    8388611.U
       c.io.in.bits.result.exponent  poke    114.S
       c.io.in.bits.result.sign	     poke    1.U
       c.io.in.bits.fcsr.foreach(_.poke(0.U))
       c.io.in.bits.op		     poke    FPUOpcode.FCVT_WU_S
       c.io.in.valid		     poke    1.B
       c.clock.step(10)
       c.io.out.bits.rd		     expect  0.U
       c.io.out.bits.fcsr(4)	     expect  1.U
       c.io.out.valid		     expect  1.U

       }
   }
} 
